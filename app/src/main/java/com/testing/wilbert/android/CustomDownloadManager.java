package com.testing.wilbert.android;

import android.app.DownloadManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;

public class CustomDownloadManager {

    private static CustomDownloadManager instance;

    Context context;
    static private ArrayList<DownloadObject> downloadQueue = new ArrayList<>();

    public static final String CHANNEL_ID = "reon_channel_id";


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "reon_channel";
            String description = "channel for reon download notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static CustomDownloadManager newInstance(Context context){

        if(instance == null){
            instance =  new CustomDownloadManager();
        }

        instance.setContext(context);
        return instance;

    }

    public void createTestNotification(){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.reon_logo)
                .setContentTitle("Hello")
                .setContentText("Test, hello this is a test message")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(1, mBuilder.build());
    }

    public void createDownloadStartNotification(String titleName, int chapter, Context context){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.reon_logo)
                .setContentTitle("Download Starting")
                .setContentText("Starting to download " + titleName +  " chapter " + chapter)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Starting to download " + titleName +  " chapter " + chapter))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(1, mBuilder.build());
    }

    public static void notifyDownloadSuccess(boolean status, String titleName, int chapter, Context context){

        String title = status ? "Download Success" : "Download Fail";
        String description = titleName +  " chapter " + chapter + " is successfully downloaded";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.reon_logo)
                .setContentTitle(title)
                .setContentText(description)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(description))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(1, mBuilder.build());

    }

    public boolean addToQueue(DownloadObject downloadObject){

        if(checkIfExists(downloadObject)){
            return false;
        }
        downloadQueue.add(downloadObject);
        createDownloadStartNotification(downloadObject.getTitleName(), downloadObject.getChapter(), context);
        return true;
    }

    private boolean checkIfExists(DownloadObject downloadObject){
        for(DownloadObject iDo : downloadQueue){
            if(iDo.equals(downloadObject)){
                return true;
            }
        }
        return false;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}

class DownloadObject{

    public static final int DOWNLOAD_COMPLETE_SUCCESS = 8;
    public static final int DOWNLOAD_IN_PROGRESS = 9;
    public static final int DOWNLOAD_COMPLETE_FAIL = 10;

    private final String titleName;
    private final int chapter;
    private int successTotal = 0;
    private int failTotal = 0;
    private final long queueID[];
    private Context context;

    public DownloadObject(String titleName, int chapter, long queueID[], Context context) {
        this.titleName = titleName;
        this.chapter = chapter;
        this.queueID = queueID;

        IntentFilter downloadCompleteIntentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        context.registerReceiver(broadcastReceiver, downloadCompleteIntentFilter);

    }

    @Override
    public boolean equals(Object object) {
        DownloadObject downloadObject = (DownloadObject) object;
        return this.titleName.equals(downloadObject.titleName) && this.chapter == downloadObject.chapter;
    }

    public int checkProgress(){
        if(queueID.length == successTotal) return DOWNLOAD_COMPLETE_SUCCESS;
        if(queueID.length <= successTotal + failTotal) return DOWNLOAD_COMPLETE_FAIL;
        return DOWNLOAD_IN_PROGRESS;
    }

    private boolean isDownloadInside(long id){

        for(long q  : queueID){
            if(id == q){
                return true;
            }
        }

        return false;

    }

    private BroadcastReceiver broadcastReceiver =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


                DownloadManager.Query query = new DownloadManager.Query();

                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
//                //Log.d("ASDF", id + " is not me " + isDownloadInside(id));

                for(long q  : queueID){
                    if(id == q){
//                        //Log.d("ADSF", "inside is " +q );
                    }
                }

                if (isDownloadInside(id)) {
                    query.setFilterById(id);
                    DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                    Cursor cursor = downloadManager.query(query);
                    if(cursor == null){
                        return;
                    }
                    cursor.moveToFirst();
                    if(cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL){
//                        //Log.d("ASDF inside class", "Success Total " + successTotal);
                        successTotal++;
                    }else{
//                        //Log.d("ASDF inside class", "False Total " + failTotal);
                        failTotal++;
                    }

                    if(successTotal == queueID.length){
                        CustomDownloadManager.notifyDownloadSuccess(true, titleName, chapter, context);
                    }else if(successTotal + failTotal >= queueID.length){
                        CustomDownloadManager.notifyDownloadSuccess(false, titleName, chapter, context);
                    }

                }



        }
    };

    public BroadcastReceiver getBroadcastReceiver() {
        return broadcastReceiver;
    }

    public String getTitleName() {
        return titleName;
    }

    public int getChapter() {
        return chapter;
    }
}
