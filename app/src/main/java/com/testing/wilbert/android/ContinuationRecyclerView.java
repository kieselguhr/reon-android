package com.testing.wilbert.android;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;

public class ContinuationRecyclerView extends RecyclerView {
    public ContinuationRecyclerView(Context context) {
        super(context);
    }

    public interface OnScrollBoundListener {
        void onScrollStart();
        void onScrollEnd();
    }

    private OnScrollBoundListener onScrollEndListener;

    public void setOnScrollEndListener(OnScrollBoundListener onScrollEndListener) {
        this.onScrollEndListener = onScrollEndListener;
    }

    public ContinuationRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ContinuationRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);



    }
}
