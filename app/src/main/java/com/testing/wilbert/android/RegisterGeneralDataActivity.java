package com.testing.wilbert.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterGeneralDataActivity extends AppCompatActivity {

    private final static int REGISTER_ACTIVITY_FORMS = 123;

    @BindView(R.id.register_general_data_act__email_edit)
    EditText emailEdit;

    @BindView(R.id.register_general_data_act__password_edit)
    EditText passwordEdit;

    @BindView(R.id.register_general_data_act__username_edit)
    EditText usernameEdit;

    @BindView(R.id.register_general_data_act__terms_condition_check)
    CheckBox termsConditionCheck;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_ACTIVITY_FORMS){
            if(resultCode == RESULT_OK) {

                setResult(RESULT_OK, data);
                finish();

            }
        }

    }

    @OnClick(R.id.register_general_data_act__register_btn)
    void continueForm(){
        if(emailEdit.getText().toString().equals("") || passwordEdit.getText().toString().equals("") || usernameEdit.getText().toString().equals("") ){
            Toast.makeText(this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
        }else if(!termsConditionCheck.isChecked()){
            Toast.makeText(this, "Please accept the terms and condition", Toast.LENGTH_SHORT).show();
        }else{
            Intent intent = new Intent(this, RegisterGenderActivity.class);
            Bundle extras = new Bundle();
            extras.putString("username", usernameEdit.getText().toString());
            extras.putString("password", passwordEdit.getText().toString());
            extras.putString("email", emailEdit.getText().toString());

            intent.putExtras(extras);

            startActivityForResult(intent, REGISTER_ACTIVITY_FORMS);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_general_data);
        ButterKnife.bind(this);

        Bundle params = getIntent().getExtras();
        if(params!=null){
            usernameEdit.setText(params.getString("username"));
            emailEdit.setText(params.getString("email"));

//            usernameEdit.setEnabled(false);
            emailEdit.setEnabled(false);

        }

        getSupportActionBar().setTitle("Sign Up");




    }
}
