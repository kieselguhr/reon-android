package com.testing.wilbert.android;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterGenreActivity extends AppCompatActivity {

    @BindView(R.id.genre_register_act__recycler_view)
    RecyclerView recyclerView;

    @OnClick(R.id.genre_register_act__finish_btn)
    void finishBtn() {

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            extras = new Bundle();
        }

        JSONArray stringGenres = new JSONArray();

        for (int i = 0; i < genres.size(); i++) {
            Genre iGenre = genres.get(i);
            if (iGenre.isChecked()) {
                stringGenres.put(iGenre.getGenre());
            }
        }

        extras.putString("genres", stringGenres.toString());

        Intent intent = new Intent();
        intent.putExtras(extras);

        setResult(RESULT_OK, intent);
        finish();
    }

    ArrayList<Genre> genres;
    GenreAdapter genreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_genre);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Genre");

        genres = new ArrayList<>();
        genres.add(new Genre(R.drawable.ic_sport_logo, "Sport"));
        genres.add(new Genre(R.drawable.ic_comedy_logo, "Comedy"));
        genres.add(new Genre(R.drawable.ic_romance_logo, "Romance"));
        genres.add(new Genre(R.drawable.ic_horror_logo, "Horror"));
        genres.add(new Genre(R.drawable.ic_daily_life_logo, "Daily Life"));
        genres.add(new Genre(R.drawable.ic_fantasy_logo, "Fantasy"));
        genres.add(new Genre(R.drawable.ic_action_logo, "Action"));
        genres.add(new Genre(R.drawable.ic_scifi_logo, "Sci-Fi"));
        genres.add(new Genre(R.drawable.ic_mystery_logo, "Mystery"));
        genres.add(new Genre(R.drawable.ic_thriller_logo, "Thriller"));
        genres.add(new Genre(R.drawable.ic_drama_logo, "Drama"));
        genres.add(new Genre(R.drawable.ic_psychological_logo, "Psychological"));
        genres.add(new Genre(R.drawable.ic_supernatural_logo, "Supernatural"));
        genres.add(new Genre(R.drawable.ic_other_logo, "Other"));

        Intent params = getIntent();
        Bundle extras = params.getExtras();

        if(extras != null){
            if (extras.getString("genres") != null) {
                try {
                    JSONArray jsonArray = new JSONArray(extras.getString("genres"));

                    //Log.d("ASDF", "json array received is " + jsonArray);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        String iGenre = jsonArray.getString(i);

                        for (int j = 0; j < genres.size(); j++) {

                            Genre jGenre = genres.get(j);

                            if (jGenre.getGenre().equals(iGenre)) {
                                jGenre.setChecked(true);
                            }

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        genreAdapter = new GenreAdapter(genres);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(genreAdapter);

    }
}

class GenreAdapter extends RecyclerView.Adapter<GenreViewHolder> {

    ArrayList<Genre> genres;

    public GenreAdapter(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    Context mContext;
    LayoutInflater mLayoutInflater;

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        //Log.d("ASDF", "called");

        view = mLayoutInflater.inflate(R.layout.grid_genre, parent, false);
        return new GenreViewHolder(view, mContext);
    }

    final int colors[] = {
            R.color.lightGray,
            R.color.colorPrimary
    };

    final int textBackground[] = {
            R.drawable.white_round_grey_outline_button,
            R.drawable.pink_round_white_outline_button
    };

    @Override
    public void onBindViewHolder(@NonNull final GenreViewHolder holder, final int position) {
        holder.genreNodeCheckedTextView.setText(genres.get(position).getGenre());
        holder.genreNodeImage.setImageDrawable(mContext.getDrawable(genres.get(position).getIconResource()));

        holder.genreNodeCheckedTextView.setChecked(genres.get(position).isChecked());
        holder.genreNodeCheckedTextView.setBackground(mContext.getDrawable(textBackground[holder.genreNodeCheckedTextView.isChecked() ? 1 : 0]));
        holder.genreNodeImage.setColorFilter(mContext.getResources().getColor(colors[holder.genreNodeCheckedTextView.isChecked() ? 1 : 0]));

        holder.genreNodeCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.genreNodeCheckedTextView.toggle();
                holder.genreNodeCheckedTextView.setBackground(mContext.getDrawable(textBackground[holder.genreNodeCheckedTextView.isChecked() ? 1 : 0]));
                holder.genreNodeImage.setColorFilter(mContext.getResources().getColor(colors[holder.genreNodeCheckedTextView.isChecked() ? 1 : 0]));
                genres.get(position).setChecked(holder.genreNodeCheckedTextView.isChecked());
            }
        });

        holder.genreNodeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.genreNodeCheckedTextView.toggle();
                holder.genreNodeCheckedTextView.setBackground(mContext.getDrawable(textBackground[holder.genreNodeCheckedTextView.isChecked() ? 1 : 0]));
                holder.genreNodeImage.setColorFilter(mContext.getResources().getColor(colors[holder.genreNodeCheckedTextView.isChecked() ? 1 : 0]));
                genres.get(position).setChecked(holder.genreNodeCheckedTextView.isChecked());
            }
        });

    }

    @Override
    public int getItemCount() {
        return genres.size();
    }
}

class GenreViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.genre_grid__genre_text_view)
    CheckedTextView genreNodeCheckedTextView;

    @BindView(R.id.genre_grid__genre_image)
    ImageView genreNodeImage;

    Context mContext;

    public GenreViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}

class Genre {

    private int iconResource;
    private String genre;
    private boolean checked;

    public Genre(int iconResource, String genre) {
        this.iconResource = iconResource;
        this.genre = genre;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
