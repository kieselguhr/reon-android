package com.testing.wilbert.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterGenderActivity extends AppCompatActivity {

    private final static int REGISTER_ACTIVITY_FORMS = 123;

    @OnClick(R.id.register_gender_act__boy_icon)
    void fillFormBoy(){
        continueForm("boy");
    }

    @OnClick(R.id.register_gender_act__girl_icon)
    void fillFormGirl(){
        continueForm("girl");
    }

    void continueForm(String gender){
        Bundle extras = getIntent().getExtras();
        extras.putString("gender", gender);

        Intent intent = new Intent(this, RegisterGenreActivity.class);
        intent.putExtras(extras);
        startActivityForResult(intent, REGISTER_ACTIVITY_FORMS);

//        startActivityForResult();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_ACTIVITY_FORMS){
            if(resultCode == RESULT_OK) {

                setResult(RESULT_OK, data);
                finish();

            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_gender);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Gender");

    }
}
