package com.testing.wilbert.android;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.search_act__tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.search_act__view_pager)
    ViewPager viewPager;

    @BindView(R.id.search_act__search_edit)
    EditText searchEdit;

    @OnClick(R.id.search_act__back_btn)
    void back(){
        finish();
    }

    ComicSearchResultFragment comicSearchResultFragment;
    AuthorSearchResultFragment authorSearchResultFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        comicSearchResultFragment = ComicSearchResultFragment.newInstance(this, "");
        authorSearchResultFragment = AuthorSearchResultFragment.newInstance(this, "");

        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);

        TextView titleText = (TextView) LayoutInflater.from(this).inflate(R.layout.search_toolbar_view_pager_node, null);
        TextView authorText = (TextView) LayoutInflater.from(this).inflate(R.layout.search_toolbar_view_pager_node, null);

        final ComicModel comicModel = new ComicModel(this);

        titleText.setText("Title");
        authorText.setText("Author");

        tabLayout.getTabAt(0).setCustomView(titleText);
        tabLayout.getTabAt(1).setCustomView(authorText);


        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                comicSearchResultFragment.setQuery(searchEdit.getText().toString());
                authorSearchResultFragment.setQuery(searchEdit.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(comicSearchResultFragment, "Comic");
        adapter.addFragment(authorSearchResultFragment, "Author");

        viewPager.setAdapter(adapter);

    }


}

