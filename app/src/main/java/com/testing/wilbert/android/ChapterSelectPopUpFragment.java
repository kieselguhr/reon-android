package com.testing.wilbert.android;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChapterSelectPopUpFragment extends DialogFragment implements ComicChapterSelectAdapter.OnChapterChangeListener {

    boolean isChapterSelected[];



    interface OnChapterDownloadListener {
        void onChapterDownload(boolean isChapterSelected[]);
    }

    OnChapterDownloadListener onChapterDownloadListener;

    @Override
    public void onChapterChange(boolean isChapterSelected[]) {
        this.isChapterSelected = isChapterSelected;

        int trueCount = 0;

        for (boolean isSelected: isChapterSelected) {
            if (isSelected) trueCount++;
        }

        chapterOutOf.setText("Download "+trueCount+" Chapters of");
        chapterCount.setText(trueCount + " Chapters");

        if(trueCount > 0){
            downloadView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            downloadView.setClickable(true);
        }else{
            downloadView.setTextColor(context.getResources().getColor(R.color.lightGray));
            downloadView.setClickable(false);
        }
    }

    public static ChapterSelectPopUpFragment newInstance(ArrayList<ComicChapter> comicChapters, Comic comic, Context context, OnChapterDownloadListener onChapterDownloadListener) {

        ChapterSelectPopUpFragment chapterSelectPopUpFragment = new ChapterSelectPopUpFragment();
        chapterSelectPopUpFragment.setComicChapters(comicChapters);
        chapterSelectPopUpFragment.setComic(comic);
        chapterSelectPopUpFragment.setContext(context);
        chapterSelectPopUpFragment.setOnChapterDownloadListener(onChapterDownloadListener);
        return chapterSelectPopUpFragment;
    }

    ArrayList<ComicChapter> comicChapters;
    Comic comic;
    Context context;

    @BindView(R.id.chapters_select_pop_up_frag__chapter_out_of)
    TextView chapterOutOf;

    @BindView(R.id.chapters_select_pop_up_frag__selected_chapter_count)
    TextView chapterCount;

    @BindView(R.id.chapters_select_pop_up_frag__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.chapters_select_pop_up_frag__title)
    TextView titleView;

    @BindView(R.id.chapters_select_pop_up_frag__image_view)
    ImageView coverView;

    @BindView(R.id.chapters_select_pop_up_frag__download_text)
    TextView downloadView;

    @OnClick(R.id.chapters_select_pop_up_frag__select_all)
    void selectAllChapters(){
        comicChaptersAdapter.selectAllChapters();
    }

    @OnClick(R.id.chapters_select_pop_up_frag__download_text)
    void download(){
        onChapterDownloadListener.onChapterDownload(isChapterSelected);
        dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pop_up_fragment_chapters_select, container, false);
    }

    ComicChapterSelectAdapter comicChaptersAdapter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        comicChaptersAdapter = new ComicChapterSelectAdapter(comicChapters, this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(comicChaptersAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

        titleView.setText(comic.getTitle());
        coverView.setImageDrawable(getActivity().getDrawable(comic.getCoverID()));

        isChapterSelected = new boolean[comicChapters.size()];
        Arrays.fill(isChapterSelected, false);

    }

    public void setComicChapters(ArrayList<ComicChapter> comicChapters) {
        this.comicChapters = comicChapters;
    }

    public void setComic(Comic comic) {
        this.comic = comic;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setOnChapterDownloadListener(OnChapterDownloadListener onChapterDownloadListener) {
        this.onChapterDownloadListener = onChapterDownloadListener;
    }
}

class ComicChapterSelectAdapter extends RecyclerView.Adapter<ChapterSelectViewHolder> {

    private ArrayList<ComicChapter> comicChapters;

    private boolean isChapterSelected[];

    public void selectAllChapters(){
        for (CheckBox cb: checkBoxes) {
            cb.setChecked(true);
        }
    }

    public ComicChapterSelectAdapter(ArrayList<ComicChapter> comicChapters, OnChapterChangeListener onChapterChangeListenerListener) {
        this.comicChapters = comicChapters;
        isChapterSelected = new boolean[comicChapters.size()];
        Arrays.fill(isChapterSelected, false);
        this.onChapterChangeListener = onChapterChangeListenerListener;

    }

    public interface OnChapterChangeListener {
        void onChapterChange(boolean isChapterSelected[]);
    }

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private OnChapterChangeListener onChapterChangeListener;

    View view;

    @NonNull
    @Override
    public ChapterSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);


//        //Log.d("ASDF", "called");

        view = mLayoutInflater.inflate(R.layout.row_chapter_select, parent, false);
        return new ChapterSelectViewHolder(view, mContext);
    }

    private ArrayList<CheckBox> checkBoxes = new ArrayList<>();

    @Override
    public void onBindViewHolder(@NonNull final ChapterSelectViewHolder holder, final int position) {

        holder.checkBox.setText(ComicChapter.generateChapterTitleString(comicChapters.get(position).getChapterNumber()));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isChapterSelected[position] = holder.checkBox.isChecked();
                onChapterChangeListener.onChapterChange(isChapterSelected);
            }
        });

        checkBoxes.add(holder.checkBox);

    }

    @Override
    public int getItemCount() {
        return comicChapters.size();
    }
}

class ChapterSelectViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.chapter_select_row__check_box)
    CheckBox checkBox;

    Context mContext;

    public ChapterSelectViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}

