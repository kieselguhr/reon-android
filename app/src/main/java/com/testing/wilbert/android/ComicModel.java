package com.testing.wilbert.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Random;

public class ComicModel {

    private Context context;
    private SharedPreferences comicSharedPreference;
    private JSONArray comicsJSONArray;

    private ArrayList<Comic> comics = new ArrayList<>();
    private LinkedHashSet<String> authors = new LinkedHashSet<>();

    public ComicModel(Context context){
        this.context = context;
        comicSharedPreference = context.getSharedPreferences("comicDatabase", Context.MODE_PRIVATE);

        refreshComic();

    }

    public Comic getComicById(int id){

        if(id > comics.size()-1 || comics.size() < 0){
            return null;
        }else{
            return comics.get(id);
        }

    }

    public int getComicCount(){
        return comics.size();
    }

    public ArrayList<Comic> getComicsByIds(int ids[]){

        ArrayList<Comic> queryResult = new ArrayList<>();

        for(int i = 0; i < ids.length; i++){
            Comic iComic = getComicById(ids[i]);
            if(iComic != null){
                queryResult.add(iComic);
            }
        }

        return queryResult;

    }

    public static JSONObject convertComicToJSONObject(Comic comic){
        JSONObject comicJSON = new JSONObject();

        try {
            comicJSON.put("title", comic.getTitle());
            comicJSON.put("art_drawer", comic.getArtDrawer());
            comicJSON.put("story_writer", comic.getStoryWriter());
            comicJSON.put("cover_id", comic.getCoverID());
            comicJSON.put("genres", ComicModel.convertArrayListToJSONArray(comic.getGenres()));
            comicJSON.put("synopsis", comic.getSynopsis());
            comicJSON.put("serialization", comic.getSerialization());
            comicJSON.put("status", comic.getStatus());
            comicJSON.put("heart_count", comic.getHeartCount());

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return comicJSON;

    }

    public static Comic convertJSONObjectToComic(JSONObject comicJSON){
        try {
            JSONArray iGenresJSON = new JSONArray();

            if(comicJSON.has("genres")){
                iGenresJSON = comicJSON.getJSONArray("genres");
            }

            String genres[] = ComicModel.covertJSONArraytoArrayList(iGenresJSON);

            Comic comic = new Comic(comicJSON.getInt("cover_id"), comicJSON.getString("title"), comicJSON.getString("story_writer"), comicJSON.getString("art_drawer"),genres, comicJSON.getString("synopsis"), comicJSON.getString("serialization"), comicJSON.getString("status"), comicJSON.getInt("heart_count"));
            return comic;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray convertComicArrayListToJSONArray(ArrayList<Comic> comics) {

        JSONArray emptyJSONArray = new JSONArray();
        for(int i = 0; i < comics.size(); i++){

            Comic iComic = comics.get(i);
            JSONObject iJSON = new JSONObject();

            try {
                iJSON.put("title", iComic.getTitle());
                iJSON.put("art_drawer", iComic.getArtDrawer());
                iJSON.put("story_writer", iComic.getStoryWriter());
                iJSON.put("cover_id", iComic.getCoverID());
                iJSON.put("genres", ComicModel.convertArrayListToJSONArray(iComic.getGenres()));
                iJSON.put("synopsis", iComic.getSynopsis());
                iJSON.put("serialization", iComic.getSerialization());
                iJSON.put("status", iComic.getStatus());
                iJSON.put("heart_count", iComic.getHeartCount());

                emptyJSONArray.put(iJSON);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return emptyJSONArray;

    }

    public static ArrayList<Comic> convertJSONArrayToComicArrayList(JSONArray comicsJSONArray){

        ArrayList<Comic> comicArrayList = new ArrayList<>();

        try {
            for (int i = 0; i < comicsJSONArray.length(); i++) {
                JSONObject iComic = comicsJSONArray.getJSONObject(i);

                JSONArray iGenresJSON = new JSONArray();

                if(iComic.has("genres")){
                    iGenresJSON = iComic.getJSONArray("genres");
                }

                String genres[] = ComicModel.covertJSONArraytoArrayList(iGenresJSON);

//                //Log.d("ASDF", "initializing + " + genres.length);

                comicArrayList.add(new Comic(iComic.getInt("cover_id"), iComic.getString("title"), iComic.getString("story_writer"), iComic.getString("art_drawer"),genres, iComic.getString("synopsis"), iComic.getInt("heart_count")));

            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        return comicArrayList;

    }

    public ArrayList<Comic> getReccomendedComic(ArrayList<String> genres) {

        LinkedHashSet<Comic> queryResult = new LinkedHashSet<>();

        for(int i = 0; i < comics.size(); i++){

//            //Log.d("ASDF", "starting loop no " + i);
            boolean match = false;
            Comic iComic = comics.get(i);

            String iGenres[] = iComic.getGenres();

            for(int j = 0; j < iGenres.length ; j++){
//                //Log.d("ASDF", "genre of "+iComic.getTitle()+"is " + iGenres[j] );

                for(int k = 0; k < genres.size() ; k++){
                    if(genres.get(k).equalsIgnoreCase(iGenres[j])){
                        match = true;
                        break;
                    }
                }

                if(match){
                    break;
                }

            }

            if(match){
                queryResult.add(iComic);
            }

        }
        if(queryResult.size() < 4){
            int arr[] = Helper.sampleRandomNumbersWithoutRepetition(0, comics.size()-1, 4-queryResult.size());

            for(int i = 0 ; i < arr.length; i++){
                queryResult.add(comics.get(arr[i]));
            }
        }

        ArrayList<Comic> queryResultArrayList = new ArrayList<>(queryResult);
        Collections.shuffle(queryResultArrayList);

        return queryResultArrayList;
    }



    public ArrayList<String> searchAuthorBy(String query){

        ArrayList<String> queryResult = new ArrayList<>();

        if(query.equals("")){
            return queryResult;
        }

        for(String a : authors){
            if(Helper.containsIgnoreCase(a, query)){
                queryResult.add(a);
            }
        }

        return queryResult;

    }

    public ArrayList<Comic> searchComicBy(String title, String author, String genre) {

        ArrayList<Comic> queryResult = new ArrayList<>();

        boolean isTitle = false;
        boolean isAuthor = false;
        boolean isGenre = false;

        if(title == null){
            isTitle = true;
        }

        if(author == null){
            isAuthor = true;
        }

        if(genre == null){
            isGenre = true;
        }

        for(int i = 0; i < comics.size(); i++){

            Comic iComic = comics.get(i);

            boolean isTitleMatch = isTitle;
            boolean isAuthorMatch = isAuthor;
            boolean isGenreMatch = isGenre;

            if(!isTitleMatch){
                if(Helper.containsIgnoreCase(iComic.getTitle(), title)){
                    isTitleMatch = true;
                }
            }

            if(!isAuthorMatch){
                if(Helper.containsIgnoreCase(iComic.getArtDrawer(), author) || Helper.containsIgnoreCase(iComic.getStoryWriter(), author)){
                    isAuthorMatch = true;
                }
            }


            if(!isGenreMatch){
                String genres[] = iComic.getGenres();

                for(int j = 0; j < genres.length ; j++){
//                    //Log.d("ASDF", "genre is " + genres[j] );
                    if(genres[j].equalsIgnoreCase(genre)){
                        isGenreMatch = true;
                        break;
                    }
                }
            }

            if(isAuthorMatch && isGenreMatch && isTitleMatch){
                queryResult.add(iComic);
//                //Log.d("ASDF", "result is " + iComic.getTitle());
            }


        }

        return queryResult;
//        return comics;
    }

    private void updateSharedPreference() {

        SharedPreferences.Editor editor = comicSharedPreference.edit();
        editor.putString("comics", comicsJSONArray.toString());
        editor.commit();

    }

    private void addNewComic(Comic comic) {
        comics.add(comic);

        JSONObject customUserJSON = new JSONObject();
        try {
            customUserJSON.put("title", comic.getTitle());
            customUserJSON.put("art_drawer", comic.getArtDrawer());
            customUserJSON.put("story_writer", comic.getStoryWriter());
            customUserJSON.put("cover_id", comic.getCoverID());
            customUserJSON.put("genres", ComicModel.convertArrayListToJSONArray(comic.getGenres()));
            customUserJSON.put("synopsis", comic.getSynopsis());
            customUserJSON.put("serialization", comic.getSerialization());
            customUserJSON.put("status", comic.getStatus());
            customUserJSON.put("heart_count", comic.getHeartCount());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        comicsJSONArray.put(customUserJSON);
        updateSharedPreference();
        refreshComic();
    }

    public void clear() {
        SharedPreferences.Editor editor = comicSharedPreference.edit();
        editor.putString("comics", "[]");
        editor.commit();
        refreshComic();
    }

    public void initializeComics(){
        clear();
        if(comicSharedPreference.getString("comics","").equals("[]")){

            //Log.d("ASDF", "intiliazing comics");

            String defaultSynopsis = context.getString(R.string.synopsis_default_desc);

            Random rand = new Random();

            int n = rand.nextInt(100);

            addNewComic(new Comic(R.drawable.cover_tarung_legenda, "Tarung Legenda", "Ockto Baringbing", "Dhang Ayupratomo", new String[]{"Action"}, context.getString(R.string.synopsis_tarung_legenda), "Online", "Bersambung",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_kris, "Kris", "Yudha Negara Nyoman", "Aloysius Alfa", new String[] {"Action"}, defaultSynopsis , "", "Bersambung",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_le_scenario, "Le Scenario", "Dominic Brian", "Istri Agung Maharani & Ruth Anastasia Setyawan", new String[]{"Action"}, context.getString(R.string.synopsis_le_scenario), "Online", "Selesai",rand.nextInt(100)  ));
            addNewComic(new Comic(R.drawable.cover_pacet, "Pacet", "Bambang Sugiarto", "Bambang Sugiarto", new String[] {"Action", "Sci-Fi"}, context.getString(R.string.synopsis_pacet), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_bayangkara, "Bayangkhara", "Isman H. Suryaman", "Anton Bandi", new String[]{"Fantasy"}, defaultSynopsis, "Online", "Bersambung",rand.nextInt(100)  ));
            addNewComic(new Comic(R.drawable.cover_the_almighty, "The Almighty", "Felix Setiawan", "Felix Setiawan", new String[]{"Action"}, context.getString(R.string.synopsis_the_almighty), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_underground_soccer_league, "Underground Soccer League", "Bambang Sugiarto", "Bambang Sugiarto", new String[] {"Sport"},context.getString(R.string.synopsis_underground_soccer_league), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_tomato_complex, "Tomato Complex", "Andri Evaris", "Andri Evaris", new String[]{"Fantasy", "Daily Life"}, context.getString(R.string.synopsis_tomato_complex), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_chrysalis, "Chrysalis", "Yudha Negara Nyoman", "Lius Lasahido, Ario Murti, Ignatius Budi", new String[]{"Action"}, context.getString(R.string.synopsis_chrysalis), "Online", "Bersambung",rand.nextInt(100)  ));
            addNewComic(new Comic(R.drawable.cover_merdeka, "MERDEKA di Bukit Selarong", "Ockto Baringbing", "Bagus Seta", new String[]{"Action","Fantasy"}, context.getString(R.string.synopsis_merdeka_di_bukit_selarong), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_nan_nans_daily_life, "Nan-nan's Daily Life", "Nan-Nan", "Nan-Nan", new String[]{"Daily Life", "Comedy"}, context.getString(R.string.synopsis_nan_nans_daily_life), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_nan_nans_daily_life_2, "Nan-nan's Daily Life 2", "Nan-Nan", "Nan-Nan", new String[]{"Daily Life", "Comedy"}, context.getString(R.string.synopsis_nan_nans_daily_life_2), "Online", "Selesai",rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_pulang, "Pulang", "Yudha Negara Nyoman", "Novian Kurnia", new String[]{"Mystery"}, context.getString(R.string.synopsis_pulang), "Online", "Selesai",rand.nextInt(100)  ));
            addNewComic(new Comic(R.drawable.cover_puppy_love, "Puppy Love", "Yudha Negara Nyoman", "LiLLac", new String[]{"Action"}, context.getString(R.string.synopsis_puppy_love), "Online", "Selesai", rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_reon_origin, "re:ON Origin", "Chris Lie", "Galang Tirtakusuma", new String[]{"Other"}, context.getString(R.string.synopsis_reon_origin), "Online", "Selesai", rand.nextInt(100)  ));
            addNewComic(new Comic(R.drawable.cover_terjebak_nostalgia, "Terjebak Nostalgia", "Titien Wattimena", "Beatrice N & Nadia K", new String[]{"Romance"}, context.getString(R.string.synopsis_terjeback_nostalgia), "Online", "Selesai", rand.nextInt(100) ));
            addNewComic(new Comic(R.drawable.cover_white_magic_vs_black_magic, "White Magic VS Black Magic", "Kurai Tao", "Takashiro", new String[]{"Comedy","Fantasy"}, defaultSynopsis, "Online", "Bersambung", rand.nextInt(100) ));
            ////Log.d("ASDF", "the json array is " + comicsJSONArray.toString());

        }else{
            //Log.d("ASDF", "comic is already tehre");
        }
    }

    public void refreshComic() {

        try {
            comicsJSONArray = new JSONArray("[]");

            String comicSharedPreferenceString = "[]";
            authors.clear();

            if (comicSharedPreference.getString("comics", null) == null) {

                SharedPreferences.Editor edit = comicSharedPreference.edit();
                edit.putString("comics", "[]");
                edit.commit();

            } else {
                comicSharedPreferenceString = comicSharedPreference.getString("comics", null);
            }
            comicsJSONArray = new JSONArray(comicSharedPreferenceString);


//            //Log.d("ASDF", "full comic array is " + comicsJSONArray);

            for (int i = 0; i < comicsJSONArray.length(); i++) {
                JSONObject iComic = comicsJSONArray.getJSONObject(i);

                JSONArray iGenresJSON = new JSONArray();

                if(iComic.has("genres")){
                    iGenresJSON = iComic.getJSONArray("genres");
                }

                String genres[] = ComicModel.covertJSONArraytoArrayList(iGenresJSON);
                authors.add(iComic.getString("art_drawer"));
                authors.add(iComic.getString("story_writer"));
//                //Log.d("ASDF", "initializing + " + genres.length);

                comics.add(new Comic(iComic.getInt("cover_id"), iComic.getString("title"), iComic.getString("story_writer"), iComic.getString("art_drawer"),genres, iComic.getString("synopsis"), iComic.getString("serialization"), iComic.getString("status"), iComic.getInt("heart_count")));

            }

        } catch (JSONException e) {
            e.printStackTrace();


        }
    }

    public static JSONArray convertArrayListToJSONArray(String[] arrayList){

        JSONArray jsonArray = new JSONArray();

        for(int i = 0; i < arrayList.length ; i++){

            jsonArray.put(arrayList[i]);

        }

        return jsonArray;

    }

    public static String[] covertJSONArraytoArrayList(JSONArray jsonArray) {
        String genreArray[] = new String[jsonArray.length()];
        try {
            for (int i = 0; i < jsonArray.length(); i++) {

                String iGenre = jsonArray.getString(i);

                genreArray[i] = iGenre;
            }

            return genreArray;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getComicById(String comicTitle){
        try {
            for (int i = 0; i < comicsJSONArray.length(); i++) {

                String iTitle = comicsJSONArray.getJSONObject(i).getString("title");

                if (iTitle.equals(comicTitle)) {
                    return i;
                }

            }
            return -1;
        } catch (JSONException e) {
            e.printStackTrace();
            return -2;
        }
    }

    public static ArrayList<Comic> sortComicAlhpabetically(ArrayList<Comic> comics){

        ArrayList<Comic> result = new ArrayList<>();

        for(int i = 0; i < comics.size(); i++){

            boolean added = false;

            Comic iComic = comics.get(i);

            String title = iComic.getTitle();

            Character iFirstLetter = title.charAt(0);


            for(int j = 0; j < result.size(); j++){

                Character jFirstLetter = result.get(j).getTitle().charAt(0);
                if(jFirstLetter >  iFirstLetter){
                    result.add(j, iComic);
                    added = true;
                    break;
                }

            }

            if(!added){
                result.add(iComic);

            }

        }

        return result;

    }

    public ArrayList<Comic> getEditorChoiceComic(){
        ArrayList<Comic> arrayList = new ArrayList<>();
        arrayList.add(comics.get(0));
        arrayList.add(comics.get(1));
        arrayList.add(comics.get(2));
        arrayList.add(comics.get(3));
        return arrayList;
    }

    public ArrayList<Comic> getNewestUpdateComic() {
        ArrayList<Comic> arrayList = new ArrayList<>();
        for(int i = comics.size()-1; i > comics.size()-6; i--){
            arrayList.add(comics.get(i));
        }
        return  arrayList;
    }

    public int getComicById(Comic comic){

        String comicTitle = comic.getTitle();

        try {
            for (int i = 0; i < comicsJSONArray.length(); i++) {

                String iTitle = comicsJSONArray.getJSONObject(i).getString("title");

                if (iTitle.equals(comicTitle)) {
                    return i;
                }

            }
            return -1;
        } catch (JSONException e) {
            e.printStackTrace();
            return -2;
        }
    }

}

class Comic{

    private int coverID;

    private String title;
    private String storyWriter;
    private String artDrawer;

    private String synopsis;

    private String genres[];

    private int heartCount;

    private String serialization = "Online";
    private String status = "Bersambung";

    public Comic(int coverID, String title, String storyWriter, String artDrawer, String genres[], String synopsis, int heartCount) {
        this.coverID = coverID;
        this.title = title;
        this.storyWriter = storyWriter;
        this.artDrawer = artDrawer;
        this.genres = genres;
        this.synopsis = synopsis;
        this.heartCount = heartCount;
    }

    public Comic(int coverID, String title, String storyWriter, String artDrawer, String[] genres, String synopsis, String serialization, String status, int heartCount) {
        this.coverID = coverID;
        this.title = title;
        this.storyWriter = storyWriter;
        this.artDrawer = artDrawer;
        this.synopsis = synopsis;
        this.genres = genres;
        this.serialization = serialization;
        this.status = status;
        this.heartCount = heartCount;
    }

    public String getSerialization() {
        return serialization;
    }

    public void setSerialization(String serialization) {
        this.serialization = serialization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public int getCoverID() {
        return coverID;
    }

    public void setCoverID(int coverID) {
        this.coverID = coverID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStoryWriter() {
        return storyWriter;
    }

    public void setStoryWriter(String storyWriter) {
        this.storyWriter = storyWriter;
    }

    public String getArtDrawer() {
        return artDrawer;
    }

    public void setArtDrawer(String artDrawer) {
        this.artDrawer = artDrawer;
    }

    public String[] getGenres() {
        return genres;
    }


    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    public int getHeartCount() {
        return heartCount;
    }

    public void setHeartCount(int heartCount) {
        this.heartCount = heartCount;
    }
}