package com.testing.wilbert.android;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class GenreDetailFragment extends Fragment {

    @BindView(R.id.genre_detail_frag__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.gender_detail_frag__comic_count)
    TextView comicCountText;

    public GenreDetailFragment() {
        // Required empty public constructor
    }

    private ArrayList<Comic> comics;
    private String genre;

    public static GenreDetailFragment newInstance(ArrayList<Comic> comics){
        GenreDetailFragment genreDetailFragment = new GenreDetailFragment();
        genreDetailFragment.setComics(comics);
        return genreDetailFragment;
    }

    public static GenreDetailFragment newInstance(String genre){
        GenreDetailFragment genreDetailFragment = new GenreDetailFragment();
        genreDetailFragment.setGenre(genre);
        return genreDetailFragment;
    }

    public ArrayList<Comic> getComics() {
        return comics;
    }

    public void setComics(ArrayList<Comic> comics) {
        this.comics = comics;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_genre_detail, container, false);
    }

    @BindView(R.id.genre_detail_frag__progress_bar)
    RelativeLayout progressBar;

    @Override
    public void onStart() {
        super.onStart();
        fillContent();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        fillContent();

    }

    void fillContent(){
        progressBar.setVisibility(View.VISIBLE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);

        ComicModel comicModel = new ComicModel(getContext());
        comics = comicModel.searchComicBy(null, null, genre);

        ImageStripDetailAdapter imageStripDetailAdapter = new ImageStripDetailAdapter(comics);
        recyclerView.setAdapter(imageStripDetailAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);
        comicCountText.setText(comics.size() + " Comics");

        progressBar.setVisibility(View.GONE);

    }


    public void setGenre(String genre) {
        this.genre = genre;
    }
}
