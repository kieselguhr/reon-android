package com.testing.wilbert.android;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DownloadedComicDetailActivity extends AppCompatActivity {

    @BindView(R.id.comic_detail_act__comic_title)
    TextView titleText;

    @BindView(R.id.comic_detail_act__art_drawer)
    TextView artDrawerText;

    @BindView(R.id.comic_detail_act__story_writer)
    TextView storyWriterText;

    @BindView(R.id.comic_detail_act__genres)
    TextView genresText;

    @BindView(R.id.comic_detail_act__comic_image)
    ImageView imageView;

    @BindView(R.id.comic_detail_act__synopsis)
    TextView synopsisText;

    @BindView(R.id.comic_detail_act__serialization)
    TextView serializationText;

    @BindView(R.id.comic_detail_act__status)
    TextView statusText;

    @BindView(R.id.custom_toolbar)
    LinearLayout toolbar;

    @BindView(R.id.comic_detail_act__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.comic_detail_act__no_chapter_text)
    TextView noChapterText;

    final static int MY_PERMISSIONS_REQUEST_STORAGE = 1243;

    @OnClick(R.id.comic_detail_act__read_btn)
    void openReader() {
        Intent intent = new Intent(this, ComicReadActivity.class);

        if (comic.getTitle().equals("Tarung Legenda") || comic.getTitle().equals("Kris") || comic.getTitle().equals("Le Scenario") || comic.getTitle().equals("Pacet")) {
            Bundle extras = new Bundle();
            extras.putString("title", comic.getTitle());
            extras.putInt("chapter", 0);
            extras.putInt("chapter_count", comicChapters.size());

            intent.putExtras(extras);

            startActivity(intent);
        } else {
            Toast.makeText(this, "Chapters are unavaibale", Toast.LENGTH_SHORT).show();
        }


    }

    Comic comic;
    ComicModel comicModel;
    UserDatabase userDatabase;

    @BindView(R.id.comic_detail_act__favourite_btn)
    LinearLayout favouriteView;

    @BindView(R.id.comic_detail_act__favourite_btn_txt)
    TextView favouriteViewText;

    @BindView(R.id.comic_detail_act__favourite_btn_ic)
    ImageView favouriteViewIcon;

    private boolean isFavourite = false;

    @OnClick(R.id.comic_detail_act__favourite_btn)
    void toggleFavouriteButton() {

        isFavourite = !isFavourite;

        if (isFavourite) {
            favouriteView.setBackground(getDrawable(R.drawable.white_round_pink_outline_button));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                favouriteViewText.setTextColor(getColor(R.color.colorPrimary));
                favouriteViewIcon.setColorFilter(getColor(R.color.colorPrimary));
            } else {
                favouriteViewText.setTextColor(getResources().getColor(R.color.colorPrimary));
                favouriteViewIcon.setColorFilter(getResources().getColor(R.color.colorPrimary));
            }
            userDatabase.addFavouritesToUser(userDatabase.getActiveUsername(), comicModel.getComicById(comic));

        } else {
            favouriteView.setBackground(getDrawable(R.drawable.pink_round_button));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                favouriteViewText.setTextColor(getColor(R.color.white));
                favouriteViewIcon.setColorFilter(getColor(R.color.white));
            } else {
                favouriteViewText.setTextColor(getResources().getColor(R.color.white));
                favouriteViewIcon.setColorFilter(getResources().getColor(R.color.white));

            }
            userDatabase.removeFavouriteFromUser(userDatabase.getActiveUsername(), comicModel.getComicById(comic));

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }


    SharedPreferences settingsPreference;
    BroadcastReceiver broadcastReceiver;

    private void downloadImages(final String[] imageLinks, int chapter) {

        DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        final int[] count = {0};

        for (int i = 0; i < imageLinks.length; i++) {
            Uri uri = Uri.parse(imageLinks[i]);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            if(i == imageLinks.length-1){
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }else{
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
            }

            if (!isFileExists("/.reon/" + comic.getTitle() + "/" + chapter + "/" + i + extension)) {
                request.setDestinationInExternalPublicDir(Environment.getRootDirectory().getAbsolutePath(), ".reon/" + comic.getTitle() + "/" + chapter + "/" + i + extension);


//                //Log.d("ASDF", "cellular downlaod "+ settingsPreference.getBoolean("cellular_download", false));


                if (!settingsPreference.getBoolean("cellular_download", false)) {
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                    //Log.d("ASDF", "cellular downlaod " + settingsPreference.getBoolean("cellular_download", false));
                    WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    if (!wifi.isWifiEnabled()) {
                        Toast.makeText(this, "Downloading on cellular network is turned off", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
                }

                downloadManager.enqueue(request);
                //Log.d("ASDF", Environment.getRootDirectory().getAbsolutePath());
            } else {
                //Log.d("ASDF", "File is existing");

            }

        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (getResultCode() == Activity.RESULT_OK) {
                    //Log.d("ASDF", "OKE");
                } else {
                    //Log.d("ASDF", "tidak oke");
                }

                count[0]++;
                //Log.d("ASDF", "one is complete");
                if (count[0] >= imageLinks.length) {
                    Toast.makeText(context, "All is done", Toast.LENGTH_SHORT).show();
                }
            }
        };

        //registerReceiver(broadcastReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @OnClick(R.id.comic_detail_act__download)
    void onDownloadPressed() {

        ChapterSelectPopUpFragment chapterSelectPopUpFragment = ChapterSelectPopUpFragment.newInstance(comicChapters, comic, this, new ChapterSelectPopUpFragment.OnChapterDownloadListener() {
            @Override
            public void onChapterDownload(boolean isChapterSelected[]) {
                download(isChapterSelected);
            }
        });
        chapterSelectPopUpFragment.show(getFragmentManager(), "ASDF");

    }

    void download(boolean isChapterSelected[]) {

        if (comic.getTitle().equals("Tarung Legenda") || comic.getTitle().equals("Kris") || comic.getTitle().equals("Le Scenario") || comic.getTitle().equals("Pacet")) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE);

            } else {

                for (int i = 0; i < chapterLinks.length; i++) {

                    if (isChapterSelected[i]) {
                        String baseLink = chapterLinks[i];

                        String[] links = new String[15];

                        for (int j = 0; j < links.length; j++) {

                            String prefix = "";

                            if (isDoubleDigit && j + 1 < 10) {
                                prefix = "0";
                            }

                            links[j] = baseLink + prefix + (j + 1) + extension;
                        }

                        downloadImages(links, i + 1);

                    }

                }

            }
        } else {
            Toast.makeText(this, "Chapters are unavaibale", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean isFileExists(String filename) {

        File folder1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + Environment.getRootDirectory() + filename);
        return folder1.exists();

    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (broadcastReceiver != null) {
//            unregisterReceiver(broadcastReceiver);
//        }
    }

    ArrayList<ComicChapter> comicChapters;
    String chapterLinks[];
    String extension = "";
    boolean isDoubleDigit = false;

    private void getDownloadedChapters() {
        String path = Environment.getExternalStorageDirectory() + "/" + Environment.getRootDirectory().getAbsolutePath() + "/.reon/" + comic.getTitle();

        comicChapters = new ArrayList<>();

        File f = new File(path);
        File[] files = f.listFiles();

        if (files != null) {
//            //Log.d("ASDF","file length is " + files.length + " vs " + chapterLinks.length + " ////" + chapterLinks[0]);
            for (File inFile : files) {
                if (inFile.isDirectory()) {
                    //Log.d("ASDF", "aaa " + inFile.getName());
                    int chapterNumber = Integer.parseInt(inFile.getName());
                    String prefix = "";

                    if (isDoubleDigit) {
                        prefix = "0";
                    }
//                    comicChapters.add(new ComicChapter(i + 1, "June 2, 2018", chapterLinks[i] + prefix + "1" + extension, chapterLinks[i]));
                    comicChapters.add(new ComicChapter(chapterNumber, "June 2, 2018",chapterLinks[chapterNumber-1]+ prefix + "1" + extension, chapterLinks[chapterNumber-1]));
                }
            }
        }


//        ImageStripDetailAdapter imageStripDetailAdapter = new ImageStripDetailAdapter(comics, DownloadedComicDetailActivity.class);

        DownloadedComicChaptersAdapter comicChaptersAdapter = new DownloadedComicChaptersAdapter(comicChapters, extension, comic.getTitle());

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(comicChaptersAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_detail);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        Bundle params = getIntent().getExtras();

        settingsPreference = this.getSharedPreferences("settings", Context.MODE_PRIVATE);

        comicModel = new ComicModel(this);
        userDatabase = new UserDatabase(this);

        noChapterText.setVisibility(View.GONE);
        try {
            comic = ComicModel.convertJSONObjectToComic(new JSONObject(params.getString("comic")));

            artDrawerText.setText(comic.getArtDrawer());
            storyWriterText.setText(comic.getStoryWriter());

            ArrayList<String> genreString = new ArrayList<>(Arrays.asList(comic.getGenres()));

            genresText.setText(genreString.toString());
            imageView.setImageDrawable(getDrawable(comic.getCoverID()));
            synopsisText.setText(comic.getSynopsis());

            statusText.setText(comic.getStatus());
            serializationText.setText(comic.getSerialization());

        } catch (JSONException e) {
            e.printStackTrace();

        }

        switch (comic.getTitle()) {
            case "Tarung Legenda":
                chapterLinks = getResources().getStringArray(R.array.tarung_legenda_chapters_png);
                extension = ".png";
                break;
            case "Kris":
                chapterLinks = getResources().getStringArray(R.array.kris_chapters_jpg);
                extension = ".jpg";
                break;
            case "Le Scenario":
                chapterLinks = getResources().getStringArray(R.array.le_scenario_chapters_png);
                extension = ".png";
                isDoubleDigit = true;
                break;
            case "Pacet":
                chapterLinks = getResources().getStringArray(R.array.pacet_chapters_png);
                extension = ".png";
                isDoubleDigit = true;
                break;
            default:
                chapterLinks = new String[0];
        }



        if (userDatabase.isComicFavourite(userDatabase.getActiveUsername(), comicModel.getComicById(comic))) {
            toggleFavouriteButton();
        }

        titleText.setText(comic.getTitle());
        titleText.setFocusable(true);

        Helper.fitTextView(titleText, 20, 20);

        comicChapters = new ArrayList<>();

        ImageView searchButton = toolbar.findViewById(R.id.main_toolbar__search_btn);
        searchButton.setClickable(true);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DownloadedComicDetailActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        ImageView backButton = toolbar.findViewById(R.id.main_toolbar__back_btn);
        backButton.setVisibility(View.VISIBLE);
        backButton.setClickable(true);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadedComicDetailActivity.this.finish();
            }
        });


        SharedPreferences.Editor editor = settingsPreference.edit();

        editor.putString("current_orientation", settingsPreference.getString("reader_orientation", "HORIZONTAL"));
        editor.apply();

        getDownloadedChapters();
    }

}

class DownloadedComicChaptersAdapter extends RecyclerView.Adapter<ComicChapterViewHolder> {

    private ArrayList<ComicChapter> comicChapters;

    public DownloadedComicChaptersAdapter(ArrayList<ComicChapter> comicChapters, String extension, String title) {
        this.comicChapters = comicChapters;
        this.extension = extension;
        this.title = title;
    }

    public String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private String extension;

    View view;

    @NonNull
    @Override
    public ComicChapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);


//        //Log.d("ASDF", "called");

        view = mLayoutInflater.inflate(R.layout.row_chapters, parent, false);
        return new ComicChapterViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull final ComicChapterViewHolder holder, final int position) {

        holder.chapterDateView.setText(comicChapters.get(position).getChapterDate());
        holder.chapterNumberView.setText(ComicChapter.generateNumberChapter(comicChapters.get(position).getChapterNumber()));
        holder.chapterTitleView.setText(ComicChapter.generateChapterTitleString(comicChapters.get(position).getChapterNumber()));

        Glide.with(mContext).load(comicChapters.get(position).getLink())
                .thumbnail(0.5f)
                .apply(new RequestOptions()
                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(holder.imageView);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DownloadedComicReadActivity.class);

                int comicChapterNumbers[] = new int[comicChapters.size()];

                for(int i=0; i < comicChapters.size(); i++){
                    comicChapterNumbers[i] = comicChapters.get(i).getChapterNumber();
                }

                Bundle extras = new Bundle();
                extras.putIntArray("chapters", comicChapterNumbers);
                extras.putString("title", title);
                extras.putInt("chapter", comicChapterNumbers[position]);
                extras.putInt("chapter_order", position);

                intent.putExtras(extras);

                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return comicChapters.size();
    }
}

