package com.testing.wilbert.android;

import android.util.TypedValue;
import android.widget.TextView;

import java.util.Random;

public class Helper {

    public static boolean containsIgnoreCase(String str, String searchStr)     {
        if(str == null || searchStr == null) return false;

        final int length = searchStr.length();
        if (length == 0)
            return true;

        for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }
        return false;
    }

    public static int[] sampleRandomNumbersWithoutRepetition(int start, int end, int count) {
        Random rng = new Random();

        int[] result = new int[count];
        int cur = 0;
        int remaining = end - start;
        for (int i = start; i < end && count > 0; i++) {
            double probability = rng.nextDouble();
            if (probability < ((double) count) / (double) remaining) {
                count--;
                result[cur++] = i;
            }
            remaining--;
        }
        return result;
    }

    public static TextView fitTextView(TextView textView, int textCountLimit, int endingSize){

        if(textView.getText().toString().length() > textCountLimit){

            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, endingSize);

        }
        return textView;
    }

    public static TextView dottedTextView(TextView textView, int textCountLimit){

        if(textView.getText().toString().length() > textCountLimit){

            String content = textView.getText().toString();
            content = content.substring(0, textCountLimit - 5) + ".....";

            textView.setText(content);

        }
        return  textView;
    }

}
