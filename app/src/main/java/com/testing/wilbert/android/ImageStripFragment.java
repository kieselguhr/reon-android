package com.testing.wilbert.android;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageStripFragment extends Fragment {

    public final static int IMAGE_STRIP_FRAGMENT_MODE_EDITOR_CHOICE = 0;
    public final static int IMAGE_STRIP_FRAGMENT_MODE_RECCOMENDED = 1;
    public final static int IMAGE_STRIP_FRAGMENT_MODE_NEWEST_UPDATE = 2;

    @BindView(R.id.image_strip_frag__title)
    TextView titleTextView;

    @BindView(R.id.image_strip_frag__descriptor)
    TextView descriptorTextView;

    @OnClick(R.id.image_strip_frag__show_all_btn)
    void showAll(){

//        Intent intent = new Intent(getActivity(), ImageStripDetailActivity.class);
////        Bundle extras = new Bundle();
////
////        extras.putString("title", title);
////        extras.putString("comics", ComicModel.convertComicArrayListToJSONArray(comics).toString());
////        intent.putExtras(extras);
////        startActivity(intent);
        homeFragment.swapToAImageStripDetailFragment(title, mode);
//        //Log.d("ASDF", "Show all is pressed");


    }

    @BindView(R.id.image_strip_frag__recycler_view)
    RecyclerView recyclerView;

    private String title;
    private String descriptor;

    private int mode;

    private HomeFragment homeFragment;

    public ImageStripFragment() {
        // Required empty public constructor
    }

    public void setTitle(String title){
        this.title = title;
    }

    public static ImageStripFragment newInstance(String title, int mode, HomeFragment homeFragment){
        ImageStripFragment imageStripFragment = new ImageStripFragment();
        imageStripFragment.setTitle(title);
        imageStripFragment.setDescriptor("");
        imageStripFragment.setMode(mode);
        imageStripFragment.setHomeFragment(homeFragment);
        return imageStripFragment;
    }

    public static ImageStripFragment newInstance(String title, String descriptor, ArrayList<Comic> comics){
        ImageStripFragment imageStripFragment = new ImageStripFragment();
        imageStripFragment.setTitle(title);
        imageStripFragment.setComics(comics);
        imageStripFragment.setDescriptor(descriptor);
        return imageStripFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_strip, container, false);
    }

    ComicNodeAdapter comicNodeAdapter;

    public void setComics(ArrayList<Comic> comics){
        this.comics = comics;
    }

    ArrayList<Comic> comics;

    public ArrayList<Comic> getComics() {
        return comics;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        titleTextView.setText(title);
        descriptorTextView.setText(descriptor);

        ComicModel comicModel = new ComicModel(getContext());
        UserDatabase userDatabase = new UserDatabase(getContext());

        ArrayList<Comic> comicNodes;

        switch (mode){
            case IMAGE_STRIP_FRAGMENT_MODE_EDITOR_CHOICE:
                comicNodes = comicModel.getEditorChoiceComic();
                break;
            case IMAGE_STRIP_FRAGMENT_MODE_RECCOMENDED:
                comicNodes = comicModel.getReccomendedComic(userDatabase.getUserGenres(userDatabase.getActiveUsername()));
                break;
            case IMAGE_STRIP_FRAGMENT_MODE_NEWEST_UPDATE:
                comicNodes = comicModel.getNewestUpdateComic();
                break;
            default:
                comicNodes = new ArrayList<>();
        }

        comicNodeAdapter = new ComicNodeAdapter(comicNodes);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 4, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(comicNodeAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

        //Log.d("ASDF", "count " + comicNodeAdapter.getItemCount());

    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public void setHomeFragment(HomeFragment homeFragment) {
        this.homeFragment = homeFragment;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}

class ComicNodeViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.image_strip_col__text)
    TextView comicTitleTextView;

    @BindView(R.id.image_strip_col__cover_image)
    ImageView coverImage;

    @BindView(R.id.image_strip_col__view)
    View rootView;

    Context mContext;

    public ComicNodeViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}

class ComicNodeAdapter extends RecyclerView.Adapter<ComicNodeViewHolder> {

    ArrayList<Comic> comicNodes;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public ComicNodeAdapter(ArrayList<Comic> comicNodes){
        this.comicNodes = comicNodes;
    }

    public ArrayList<Comic> getComicNodes() {
        return comicNodes;
    }

    public void setComicNodes(ArrayList<Comic> comicNodes) {
        this.comicNodes = comicNodes;
    }

    @NonNull
    @Override
    public ComicNodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.column_image_strip, parent, false);
        return new ComicNodeViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull ComicNodeViewHolder holder, final int position) {
        holder.coverImage.setImageDrawable(mContext.getDrawable(comicNodes.get(position).getCoverID()));
        holder.comicTitleTextView.setText(comicNodes.get(position).getTitle());

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, ComicDetailActivity.class);

                Bundle extras = new Bundle();
                extras.putString("comic", ComicModel.convertComicToJSONObject(comicNodes.get(position)).toString());
                intent.putExtras(extras);

                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {

        if(comicNodes.size() > 4){
            return 4;
        }

        return comicNodes.size();
    }
}

