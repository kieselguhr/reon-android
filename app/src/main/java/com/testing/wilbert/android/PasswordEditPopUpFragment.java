package com.testing.wilbert.android;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PasswordEditPopUpFragment extends DialogFragment {

    public static interface PasswordChangedListener{
        void onPasswordChanged(String password);
    }

    PasswordChangedListener passwordChangedListener;

    @BindView(R.id.change_password_pop_up_frag__new_password)
    EditText newPasswordEdit;

    @BindView(R.id.change_password_pop_up_frag__old_password)
    EditText oldPasswordEdit;

    @OnClick(R.id.change_password_pop_up_frag__change_btn)
    void changePassword(){
        UserDatabase userDatabase = new UserDatabase(getActivity());
        if(userDatabase.changePassword(oldPasswordEdit.getText().toString(), newPasswordEdit.getText().toString())){
           dismiss();
        }else{
            Toast.makeText(getActivity(), "Incorrect Password", Toast.LENGTH_SHORT).show();
        }
    }

    public static PasswordEditPopUpFragment newInstance(PasswordChangedListener passwordChangedListener){
        PasswordEditPopUpFragment passwordEditPopUpFragment = new PasswordEditPopUpFragment();
        passwordEditPopUpFragment.setPasswordChangedListener(passwordChangedListener);
        return passwordEditPopUpFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pop_up_change_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow()
                .setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public void setPasswordChangedListener(PasswordChangedListener passwordChangedListener) {
        this.passwordChangedListener = passwordChangedListener;
    }
}
