package com.testing.wilbert.android;


import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.CircularProgressDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComicReaderHorizontalFragment extends Fragment {


    public ComicReaderHorizontalFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.comic_reader_horizontal_frag__view_pager)
    ViewPager viewPager;

    String links[];

    boolean onlyShowDownloaded;

    String downloadedFileLocations[];


    public void setLinks(String[] links) {
        this.links = links;
    }

    public void setChangeChapterListener(ChangeChapterListener changeChapterListener) {
        this.changeChapterListener = changeChapterListener;
    }

    ChangeChapterListener changeChapterListener;

    public static ComicReaderHorizontalFragment newInstance(String[] links, String[] downloadedFileLocations, ChangeChapterListener chapterChangeListener) {
        ComicReaderHorizontalFragment comicReaderHorizontalFragment = new ComicReaderHorizontalFragment();
        comicReaderHorizontalFragment.setLinks(links);
        comicReaderHorizontalFragment.setChangeChapterListener(chapterChangeListener);
        comicReaderHorizontalFragment.setDownloadedFileLocations(downloadedFileLocations);
//        comicReaderHorizontalFragment.setOnlyShowDownloaded(onlyShowDownloaded);
        comicReaderHorizontalFragment.setOnlyShowDownloaded(false);
        return comicReaderHorizontalFragment;
    }

    public static ComicReaderHorizontalFragment newInstance(String[] downloadedFileLocations, ChangeChapterListener chapterChangeListener) {
        ComicReaderHorizontalFragment comicReaderHorizontalFragment = new ComicReaderHorizontalFragment();
        comicReaderHorizontalFragment.setChangeChapterListener(chapterChangeListener);
        comicReaderHorizontalFragment.setDownloadedFileLocations(downloadedFileLocations);
//        comicReaderHorizontalFragment.setOnlyShowDownloaded(onlyShowDownloaded);
        comicReaderHorizontalFragment.setOnlyShowDownloaded(true);
        return comicReaderHorizontalFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comic_reader_horizontal, container, false);

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

//        String[] links = {
//                "http://www.reoncomics.com/uploads/comic/320/155/kris1_1.jpg"
//        };

        LayoutInflater layoutInflater = getLayoutInflater();

        ImageView iv = (ImageView) layoutInflater.inflate(R.layout.column_horizontal_fragment_row, null);
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC );
        circularProgressDrawable.start();
        Glide.with(getContext()).load(R.drawable.ad_banner)
                .apply(new RequestOptions().downsample(DownsampleStrategy.CENTER_INSIDE)
                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(circularProgressDrawable).error(R.drawable.ic_broken_image))
                .thumbnail(0.5f)
                .into(iv);

        final HorizontalReaderChapterAdapter horizontalReaderChapterAdapter = new HorizontalReaderChapterAdapter(links, downloadedFileLocations);

        viewPager.addView(iv);
        viewPager.setAdapter(horizontalReaderChapterAdapter);


        ViewPager.OnPageChangeListener pagerListener = new ViewPager.OnPageChangeListener() {
            boolean lastPageChange = false;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
//                viewPager.setCurrentItem(position);

                //Log.d("ASDF", "on page " + viewPager.getCurrentItem());

                if (position == horizontalReaderChapterAdapter.getCount() - 1) {
                    //Log.d("ASDF", "LAST");
                    viewPager.setCurrentItem(position - 1);
                    changeChapterListener.moveToNextChapter();

                }

                if (position == 0) {
                    //Log.d("ASDF", "First");
                    viewPager.setCurrentItem(1);
                    changeChapterListener.moveToPreviousChapter();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                int lastIdx = horizontalReaderChapterAdapter.getCount() - 1;

                int curItem = viewPager.getCurrentItem();
                if (curItem == lastIdx && state == ViewPager.SCROLL_STATE_DRAGGING) {
                    lastPageChange = true;

                    // i put this here since onPageScroll gets called a couple of times.
                    //Log.d("ASDF", "LASTO");

                } else {
                    lastPageChange = false;
                }
            }
        };

        viewPager.setCurrentItem(1);
        viewPager.addOnPageChangeListener(pagerListener);


    }

    public void setDownloadedFileLocations(String[] downloadedFileLocations) {
        this.downloadedFileLocations = downloadedFileLocations;
    }

    public void setOnlyShowDownloaded(boolean onlyShowDownloaded) {
        this.onlyShowDownloaded = onlyShowDownloaded;
    }

    public class HorizontalReaderChapterAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        String[] links;
        String[] downloadedFileLocations;

        public HorizontalReaderChapterAdapter(String[] links, String[] downloadedFileLocations) {
            if(onlyShowDownloaded){
                this.links = downloadedFileLocations;
            }else{
                this.links = links;
            }
            this.downloadedFileLocations = downloadedFileLocations;
        }

        @Override
        public int getCount() {

            if(onlyShowDownloaded){
                return downloadedFileLocations.length+2;
            }else{
                return links.length + 2;

            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            if (position < links.length+1 && position != 0) {
                layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.column_horizontal_fragment_row, container, false);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeChapterListener.toggleTopBar();
                    }
                });

                ImageView imageViewPreview = (ImageView) view.findViewById(R.id.horizontal_fragment_reader_row__image_view);

                boolean isFromStorage = false;

                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(getContext());
                circularProgressDrawable.setStrokeWidth(5f);
                circularProgressDrawable.setCenterRadius(30f);
                circularProgressDrawable.setColorFilter(getContext().getResources().getColor(R.color.white), PorterDuff.Mode.SRC );
                circularProgressDrawable.start();

                Drawable errorDrawable = getResources().getDrawable(R.drawable.ic_broken_image);
                errorDrawable.setTint(getResources().getColor(R.color.white));
                errorDrawable.setBounds(0,0,25,25);
// Scale it to 50 x 50

                if(downloadedFileLocations.length > 0 ){

                    File file = new File(downloadedFileLocations[position-1]);
                    if(file.exists()){
                        Glide.with(getContext()).load(file)
                                .thumbnail(0.5f)
                                .apply(new RequestOptions()
                                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(circularProgressDrawable).error(errorDrawable) )
                                .into(imageViewPreview);
                        isFromStorage = true;
//                //Log.d("ASDF", " file exits");
                    }else if(!onlyShowDownloaded){
                        Glide.with(getActivity()).load(links[position-1])
                                .thumbnail(0.5f)
                                .apply(new RequestOptions()
                                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(circularProgressDrawable).error(errorDrawable)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .into(imageViewPreview);
                    }
                }else{
                    if(!isFromStorage && !onlyShowDownloaded){
                        Glide.with(getActivity()).load(links[position-1])
                                .thumbnail(0.5f)
                                .apply(new RequestOptions()
                                        .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(circularProgressDrawable).error(errorDrawable)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .into(imageViewPreview);
                    }
                }



                container.addView(view);


                return view;
            } else {

                layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.column_horizontal_fragment_row, container, false);

                ImageView imageViewPreview = (ImageView) view.findViewById(R.id.horizontal_fragment_reader_row__image_view);

                imageViewPreview.setBackgroundColor(getResources().getColor(R.color.black));

                container.addView(view);

                return view;

            }


        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }
}
