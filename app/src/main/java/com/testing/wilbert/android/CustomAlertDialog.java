package com.testing.wilbert.android;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomAlertDialog extends DialogFragment {

    public interface OnCustomAlertDialogResponseListener {
        void onPositiveButtonClicked();
        void onNegativeButtonClicked();
    }

    public static CustomAlertDialog newInstance(String title, String content, String positiveOption, String negativeOption, OnCustomAlertDialogResponseListener onCustomAlertDialogResponseListener) {
        CustomAlertDialog customAlertDialog = new CustomAlertDialog();
        customAlertDialog.setTitle(title);
        customAlertDialog.setContent(content);
        customAlertDialog.setPositiveOption(positiveOption);
        customAlertDialog.setNegativeOption(negativeOption);
        customAlertDialog.setOnCustomAlertDialogResponseListener(onCustomAlertDialogResponseListener);
        return customAlertDialog;
    }

    public static OnCustomAlertDialogResponseListener DEFAULT_LISTENER = new OnCustomAlertDialogResponseListener() {
        @Override
        public void onPositiveButtonClicked() {

        }

        @Override
        public void onNegativeButtonClicked() {

        }
    };

    private String title = "";
    private String positiveOption = "";
    private String negativeOption = "";
    private String content = "";

    public OnCustomAlertDialogResponseListener onCustomAlertDialogResponseListener;

    public CustomAlertDialog.OnCustomAlertDialogResponseListener getOnCustomAlertDialogResponseListener() {
        return onCustomAlertDialogResponseListener;
    }

    public void setOnCustomAlertDialogResponseListener(CustomAlertDialog.OnCustomAlertDialogResponseListener onCustomAlertDialogResponseListener) {
        this.onCustomAlertDialogResponseListener = onCustomAlertDialogResponseListener;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPositiveOption() {
        return positiveOption;
    }

    public void setPositiveOption(String positiveOption) {
        this.positiveOption = positiveOption;
    }

    public String getNegativeOption() {
        return negativeOption;
    }

    public void setNegativeOption(String negativeOption) {
        this.negativeOption = negativeOption;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_custom, container, false);
    }

    @BindView(R.id.custom_dialog__positive_btn)
    TextView positiveText;

    @BindView(R.id.custom_dialog__negative_btn)
    TextView negativeText;

    @BindView(R.id.custom_dialog__title_text)
    TextView titleText;

    @BindView(R.id.custom_dialog__content_text)
    TextView contentText;

    @OnClick(R.id.custom_dialog__positive_btn)
    void onPositive(){
        onCustomAlertDialogResponseListener.onPositiveButtonClicked();
        dismiss();
    }

    @OnClick(R.id.custom_dialog__negative_btn)
    void onNegative(){
        onCustomAlertDialogResponseListener.onNegativeButtonClicked();
        dismiss();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        titleText.setText(title);

        positiveText.setText(positiveOption);

        negativeText.setText(negativeOption);

        contentText.setText(content);

    }

}
