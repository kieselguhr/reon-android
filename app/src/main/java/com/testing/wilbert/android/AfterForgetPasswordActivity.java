package com.testing.wilbert.android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AfterForgetPasswordActivity extends AppCompatActivity {

    @OnClick(R.id.simple_toolbar_back_btn)
    void back(){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_forget_password);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
    }
}
