package com.testing.wilbert.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageStripDetailActivity extends AppCompatActivity {

    @BindView(R.id.image_strip_detail_act__title)
    TextView titleText;

    @BindView(R.id.image_strip_detail_act__comic_count)
    TextView comicCountText;

    @BindView(R.id.image_strip_detail_act__recycler_view)
    RecyclerView recyclerView;

    ArrayList<Comic> comics;

    @BindView(R.id.custom_toolbar)
    LinearLayout toolbar;

    @BindView(R.id.image_strip_detail_act__change_order)
    TextView changeOrder;

    @BindView(R.id.image_strip_detail_act__tab_layout)
    TabLayout tabLayout;

    @OnClick(R.id.image_strip_detail_act__change_order)
    void changeOrder() {

        String status = changeOrder.getText().toString();

        if(status.contains("Updated")){

            changeOrder.setText("Sort By : Alphabetical");

            comics = ComicModel.sortComicAlhpabetically(comics);

            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();


        }else if(status.contains("Alphabetical")){

            changeOrder.setText("Sort By : Updated");
            comics = comicModel.getComicsByIds(userDatabase.getFavouriteComicsId(userDatabase.getActiveUsername()));
            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();

        }

    }

    UserDatabase userDatabase;
    ComicModel comicModel;
    ImageStripDetailAdapter imageStripDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_strip_detail);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        Bundle params = getIntent().getExtras();

        titleText.setText(params.getString("title"));

        userDatabase = new UserDatabase(this);
        comicModel = new ComicModel(this);

        setCustomTab();

        try {

            comics = ComicModel.convertJSONArrayToComicArrayList(new JSONArray(params.getString("comics")));
            comicCountText.setText(comics.size() + " Comics");

            imageStripDetailAdapter = new ImageStripDetailAdapter(comics);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);

            recyclerView.setAdapter(imageStripDetailAdapter);
            recyclerView.setLayoutManager(gridLayoutManager);

        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d("ASDF", "Invalid parameter for JSON array");
        }

        ImageView imageView = toolbar.findViewById(R.id.main_toolbar__search_btn);
        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ImageStripDetailActivity.this, SearchActivity.class);

                Bundle extras = new Bundle();
//                extras.putString("");

                startActivity(intent);
            }
        });

    }

    private void setCustomTab() {

        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        tabLayout.setRotationX(180);
        RelativeLayout homeTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        homeTab.setRotationX(180);

        RelativeLayout discoverTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        ImageView discoverTabIcon = discoverTab.findViewById(R.id.custom_tab__icon);
        TextView discoverTabTextView = discoverTab.findViewById(R.id.custom_tab__text);

        discoverTabTextView.setText("Discover");
        discoverTabIcon.setImageDrawable(getDrawable(R.drawable.ic_discover));

        RelativeLayout favouriteTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        ImageView favouriteTabIcon = favouriteTab.findViewById(R.id.custom_tab__icon);
        TextView favouriteTabTextView = favouriteTab.findViewById(R.id.custom_tab__text);

        favouriteTabIcon.setImageDrawable(getDrawable(R.drawable.ic_favorite));
        favouriteTabTextView.setText("Favourite");

        RelativeLayout recentTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        ImageView recentTabIcon = recentTab.findViewById(R.id.custom_tab__icon);
        TextView recentTabTextView = recentTab.findViewById(R.id.custom_tab__text);

        recentTabIcon.setImageDrawable(getDrawable(R.drawable.ic_recent));
        recentTabTextView.setText("Recent");

        RelativeLayout moreTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

        ImageView moreTabIcon = moreTab.findViewById(R.id.custom_tab__icon);
        TextView moreTabTextView = moreTab.findViewById(R.id.custom_tab__text);

        moreTabIcon.setImageDrawable(getDrawable(R.drawable.ic_more));
        moreTabTextView.setText("More");

        tabLayout.getTabAt(0).setCustomView(homeTab);
        tabLayout.getTabAt(1).setCustomView(discoverTab);
        tabLayout.getTabAt(2).setCustomView(favouriteTab);
        tabLayout.getTabAt(3).setCustomView(recentTab);
        tabLayout.getTabAt(4).setCustomView(moreTab);

        homeTab.setRotationX(180);
        discoverTab.setRotationX(180);
        favouriteTab.setRotationX(180);
        recentTab.setRotationX(180);
        moreTab.setRotationX(180);

    }
}

class ImageStripDetailViewHolder extends RecyclerView.ViewHolder{

    Context mContext;

    @BindView(R.id.image_strip_detail_row__title)
    TextView titleText;

    @BindView(R.id.image_strip_detail_row__image)
    ImageView coverImage;

    @BindView(R.id.image_strip_detail_row__synopsis)
    TextView synopsisText;

    @BindView(R.id.image_strip_detail_row__heart_count)
    TextView heartCountText;

    @BindView(R.id.image_strip_detail_row__view)
    LinearLayout rootView;


    public ImageStripDetailViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}

class ImageStripDetailAdapter extends RecyclerView.Adapter<ImageStripDetailViewHolder> {

    ArrayList<Comic> comicNodes;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public ImageStripDetailAdapter(ArrayList<Comic> comicNodes){
        this.comicNodes = comicNodes;
    }

    public ImageStripDetailAdapter(ArrayList<Comic> comicNodes, Class<?> targetClass){
        this.comicNodes = comicNodes;
        this.targetClass = targetClass;
    }

    Class<?> targetClass = ComicDetailActivity.class;

    public ArrayList<Comic> getComicNodes() {
        return comicNodes;
    }


    public void setComicNodes(ArrayList<Comic> comicNodes) {
        this.comicNodes = comicNodes;
    }

    @NonNull
    @Override
    public ImageStripDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.row_image_strip_detail, parent, false);
        return new ImageStripDetailViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageStripDetailViewHolder holder, final int position) {
//        holder.coverImage.setImageDrawable(mContext.getDrawable(imageLinks.get(position).coverID));
//        holder.comicTitleTextView.setText(imageLinks.get(position).getTitle());
//
//        Random rand = new Random();
//
//        int  n = rand.nextInt(100);

        holder.coverImage.setImageDrawable(mContext.getDrawable(comicNodes.get(position).getCoverID()));
        holder.heartCountText.setText(comicNodes.get(position).getHeartCount()+"");
        holder.synopsisText.setText(comicNodes.get(position).getSynopsis());

        Helper.dottedTextView(holder.synopsisText, 120);

        holder.titleText.setText(comicNodes.get(position).getTitle());
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, targetClass);

                Bundle extras = new Bundle();
                extras.putString("comic", ComicModel.convertComicToJSONObject(comicNodes.get(position)).toString());
                intent.putExtras(extras);

                mContext.startActivity(intent);

            }
        });

    }


    @Override
    public int getItemCount() {
        if(comicNodes != null){
            return comicNodes.size();
        }
        return 0;
    }
}