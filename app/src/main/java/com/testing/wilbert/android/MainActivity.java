package com.testing.wilbert.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnFragmentChangeListener{

    @Override
    public void setCustomBackPressedListener(CustomBackPressedListener customBackPressedListener) {
        backPressedListener = customBackPressedListener;
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backPressedListener.onBackButtonPressed();
                backButton.setVisibility(View.GONE);
            }
        });
    }

//    @Override
//    protected void onStop() {
////        UserDatabase userDatabase = new UserDatabase(this);
////        userDatabase.setActiveUser("");
////        super.onStop();
//    }

    @BindView(R.id.main_toolbar__back_btn)
    ImageView backButton;

    @BindView(R.id.main_act__tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.main_act__view_pager)
    ViewPager viewPager;

    @BindView(R.id.custom_toolbar)
    LinearLayout toolbar;

    private boolean doubleBackToExitPressedOnce = false;
    private ViewPagerAdapter adapter;
    private FavouriteComicFragment favouriteComicFragment;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private void setupViewPager(ViewPager viewPager) {

        favouriteComicFragment = FavouriteComicFragment.newInstance(this);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(HomeFragment.newInstance(this, 0), "Home");
        adapter.addFragment(new DiscoverFragment(), "Discover");
        adapter.addFragment(favouriteComicFragment, "Favourite");
        adapter.addFragment(new HistoryComicFragment(), "Recent");
        adapter.addFragment(new MoreMenuFragment(), "More");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Log.d("ASDF", "activity result " + requestCode + "  " + resultCode);

        if(requestCode == MoreMenuFragment.MY_ACCOUNT_ACTIVITY_CODE && resultCode == MyAccountActivity.ACTIVITY_RESULT_LOGOUT){
            UserDatabase userDatabase = new UserDatabase(this);
            userDatabase.logOut();

            Toast.makeText(this, "Successfully Logged Out", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);

            finish();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();

        //Log.d("ASDF", "on resume called");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Objects.requireNonNull(getSupportActionBar()).hide();

        setupViewPager(viewPager);

        setCustomTab();

        UserDatabase userDatabase = new UserDatabase(this);
        CustomUser customUser = userDatabase.getActiveUser();


        if(customUser == null){
            Intent intent = new Intent(this, SignInActivity.class);
            Toast.makeText(this, "Please Login First", Toast.LENGTH_SHORT).show();
            startActivity(intent);
            finish();
        }else{
        }

        ImageView imageView = toolbar.findViewById(R.id.main_toolbar__search_btn);
        imageView.setClickable(true);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d("ASDF", "is clicked");
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });



//        placeImageStripDetailFragmentOver(0, "hi", new ArrayList<Comic>());

    }

//    public void backButtonListener(View.OnClickListener onClickListener){
//        ImageView imageView = toolbar.findViewById(R.id.main_toolbar__back_btn);
//        imageView.setOnClickListener();
//    }

    private CustomBackPressedListener backPressedListener;

    private void setCustomTab() {

        if(adapter.getCount() > 0){
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setRotationX(180);
            RelativeLayout homeTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            homeTab.setRotationX(180);

            RelativeLayout discoverTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

            ImageView discoverTabIcon = discoverTab.findViewById(R.id.custom_tab__icon);
            TextView discoverTabTextView = discoverTab.findViewById(R.id.custom_tab__text);

            discoverTabTextView.setText("Discover");
            discoverTabIcon.setImageDrawable(getDrawable(R.drawable.ic_discover));

            RelativeLayout favouriteTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

            ImageView favouriteTabIcon = favouriteTab.findViewById(R.id.custom_tab__icon);
            TextView favouriteTabTextView = favouriteTab.findViewById(R.id.custom_tab__text);

            favouriteTabIcon.setImageDrawable(getDrawable(R.drawable.ic_favorite));
            favouriteTabTextView.setText("Favourite");

            RelativeLayout recentTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

            ImageView recentTabIcon = recentTab.findViewById(R.id.custom_tab__icon);
            TextView recentTabTextView = recentTab.findViewById(R.id.custom_tab__text);

            recentTabIcon.setImageDrawable(getDrawable(R.drawable.ic_recent));
            recentTabTextView.setText("Recent");

            RelativeLayout moreTab = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);

            ImageView moreTabIcon = moreTab.findViewById(R.id.custom_tab__icon);
            TextView moreTabTextView = moreTab.findViewById(R.id.custom_tab__text);

            moreTabIcon.setImageDrawable(getDrawable(R.drawable.ic_more));
            moreTabTextView.setText("More");

            tabLayout.getTabAt(0).setCustomView(homeTab);
            tabLayout.getTabAt(1).setCustomView(discoverTab);
            tabLayout.getTabAt(2).setCustomView(favouriteTab);
            tabLayout.getTabAt(3).setCustomView(recentTab);
            tabLayout.getTabAt(4).setCustomView(moreTab);

            homeTab.setRotationX(180);
            discoverTab.setRotationX(180);
            favouriteTab.setRotationX(180);
            recentTab.setRotationX(180);
            moreTab.setRotationX(180);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if(position == 0 ){
                        if(adapter.getItem(position) instanceof HomeFragment){
                            backPressedListener = ((HomeFragment) adapter.getItem(position)).getBackPressedListener();
                            if(backPressedListener != null){
                                backButton.setVisibility(View.VISIBLE);
                                backButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        backPressedListener.onBackButtonPressed();
                                        backButton.setVisibility(View.GONE);
                                    }
                                });
                                return;
                            }
                        }
                    }
                    else if(position == 4){
                        if(adapter.getItem(position) instanceof MoreMenuFragment){
                            backPressedListener = ((MoreMenuFragment) adapter.getItem(position)).getBackPressedListener();
                            if(backPressedListener != null){
                                Log.d("ASDF", " backpressed listener  is not null");

                                backButton.setVisibility(View.VISIBLE);
                                backButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        backPressedListener.onBackButtonPressed();
                                        backButton.setVisibility(View.GONE);
                                    }
                                });
                                return;
                            }

                        }
                    }

                    backPressedListener = null;
                    backButton.setVisibility(View.GONE);

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }
    }
}

class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public void replaceFragment(Fragment fragment, String title, int index) {

    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    public void setItem(int position, Fragment fragment){
        mFragmentList.set(position, fragment);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}

interface OnFragmentChangeListener{
    void setCustomBackPressedListener(CustomBackPressedListener customBackPressedListener);
}