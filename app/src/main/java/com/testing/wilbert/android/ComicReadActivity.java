package com.testing.wilbert.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComicReadActivity extends AppCompatActivity implements ChangeChapterListener {

    boolean isVertical = true;

    @BindView(R.id.comic_read_act__orientation_swap_btn)
    ImageView swapImageView;

    String links[];
    String downloadedImageLocations[];
    String title;

    int position;
    int chapterCount = 0;

    @OnClick(R.id.comic_read_act__pressable_area)
    void onTopToolbarPressed(){
        callChapters();
    }


    private void callChapters(){

        ChapterSelectDialog chapterSelectDialog = ChapterSelectDialog.newInstance(chapterCount, position, new ChapterSelectDialog.OnChapterSelectedListener() {
            @Override
            public void onChapterSelected(int selectedChapter) {
                final Bundle params=  getIntent().getExtras();

                params.putInt("chapter", selectedChapter);

                Intent intent = new Intent(ComicReadActivity.this, ComicReadActivity.class);
                intent.putExtras(params);
                startActivity(intent);
                finish();
//                Toast.makeText(ComicReadActivity.this, "Returned Chapter is " + selectedChapter , Toast.LENGTH_SHORT).show();
            }
        });

        chapterSelectDialog.show(getFragmentManager(), "ASDF");

    }

    @OnClick(R.id.comic_read_act__back_button)
    void onBackButton(){
        finish();
    }

    @BindView(R.id.comic_read_act__chapter_detail)
    View chapterDetailView;

    boolean isTopBarVisible = true;

    //@OnClick(R.id.comic_read_act__invisible_button)
    public void toggleTopBar(){
        //Log.d("ASDF", "Toggle");
        if(isTopBarVisible){
            chapterDetailView.animate().translationY(-150);
        }else{
            chapterDetailView.animate().translationY(0);
        }
        isTopBarVisible = !isTopBarVisible;
    }

    @BindView(R.id.comic_read_act__comic_title)
    TextView titleView;

    @BindView(R.id.comic_read_act__comic_chapter_number)
    TextView chapterView;

    @OnClick(R.id.comic_Read_act__comic_read_placeholder)
    void toggle(){
        toggleTopBar();
    }

    private void prepareToolbar(){
//        //Log.d("ASDF", "title is " + title);
        titleView.setText(title);
        chapterView.setText("Chapter " + (chapterNumber + 1));
    }

    int chapterNumber;

    public void moveToPreviousChapter(){
        if(getIntent().getExtras() != null){
            final Bundle params=  getIntent().getExtras();

            String baseLink = params.getString("link");
            String extension = params.getString("extension");
            final int newPosition = params.getInt("chapter") - 1;

//            extras.putString("title", title);

            if ( newPosition < 0){
                Toast.makeText(this, "This is the first chapter", Toast.LENGTH_SHORT).show();
            }else{

                String alertTitle = "Previous Chapter";
                String content = "Do you want to move to the previous chapter ?";
                String positiveOption = "YES";
                String negativeOption = "NO";

                CustomAlertDialog customAlertDialog = CustomAlertDialog.newInstance(alertTitle, content, positiveOption, negativeOption, new CustomAlertDialog.OnCustomAlertDialogResponseListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        params.putInt("chapter", newPosition);

                        Intent intent = new Intent(ComicReadActivity.this, ComicReadActivity.class);
                        intent.putExtras(params);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onNegativeButtonClicked() {
                    }
                });
                customAlertDialog.show(getFragmentManager(), "CUSTOM_ALERT_DIALOG");
            }

        }
    }

    public void moveToNextChapter(){

        if(getIntent().getExtras() != null){
            final Bundle params=  getIntent().getExtras();

            final int newPosition = params.getInt("chapter") + 1;
            String title = params.getString("title");
//            extras.putString("title", title);

            //Log.d("ASDF", "new chapter is " + newPosition  +" ||| " +lastChapter);

            if ( newPosition >= lastChapter){
                Toast.makeText(this, "This is the last chapter", Toast.LENGTH_SHORT).show();
            }else{

                String alertTitle = "Next Chapter";
                String content = "Do you want to move to the next chapter ?";
                String positiveOption = "YES";
                String negativeOption = "NO";

                CustomAlertDialog customAlertDialog = CustomAlertDialog.newInstance(alertTitle, content, positiveOption, negativeOption, new CustomAlertDialog.OnCustomAlertDialogResponseListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        params.putInt("chapter", newPosition);

                        Intent intent = new Intent(ComicReadActivity.this, ComicReadActivity.class);
                        intent.putExtras(params);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onNegativeButtonClicked() {
                    }
                });
                customAlertDialog.show(getFragmentManager(), "CUSTOM_ALERT_DIALOG");
            }

        }

    }

    @OnClick(R.id.comic_read_act__orientation_swap_btn)
    void swapOrientation(){
        SharedPreferences settingsPreference =getSharedPreferences("settings", Context.MODE_PRIVATE);
        if(isVertical){
            swapImageView.setImageDrawable(getDrawable(R.drawable.ic_swap_vert));
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.comic_Read_act__comic_read_placeholder, ComicReaderHorizontalFragment.newInstance(links, downloadedImageLocations, this));
            fragmentTransaction.commit();
            SharedPreferences.Editor editor = settingsPreference.edit();

            editor.putString("current_orientation", "HORIZONTAL");
            editor.commit();
//            //Log.d("ADSF", "current orientation is " + settingsPreference.getString("current_orientation", "HORIZONTAL"));

        }else{
            swapImageView.setImageDrawable(getDrawable(R.drawable.ic_swap_horiz));

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.comic_Read_act__comic_read_placeholder, ComicReaderVerticalFragment.newInstance(links, this, downloadedImageLocations));
            fragmentTransaction.commit();

            SharedPreferences.Editor editor = settingsPreference.edit();

            editor.putString("current_orientation","VERTICAL");
            editor.commit();
//            //Log.d("ADSF", "current orientation is " + settingsPreference.getString("reader_orientation", "VERTICAL"));

        }
        String orientation = settingsPreference.getString("current_orientation", "HORIZONTAL");

        //Log.d("ADSF", "current orientation is " + orientation);

        isVertical = !isVertical;



    }

    int lastChapter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_read);

        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ButterKnife.bind(this);
        if(getIntent().getExtras() != null){
            final Bundle params=  getIntent().getExtras();
            title = params.getString("title");
            chapterNumber = params.getInt("chapter");
            chapterCount = params.getInt("chapter_count");
//            //Log.d("ASDF", "Chapter number is " + chapterNumber );
        }

        if(getIntent().getExtras() != null){
            Bundle params=  getIntent().getExtras();

            //String baseLink = params.getString("link");
            position = params.getInt("chapter");
            String extension = ".jpg";
            String title = params.getString("title");
            String baseLink = "";

            ComicModel comicModel = new ComicModel(this);
            UserDatabase userDatabase = new UserDatabase(this);

            userDatabase.addHistoryToUser(userDatabase.getActiveUsername(), comicModel.getComicById(title));
            boolean isDoubleDigit = false;

            switch (title){
                case "Tarung Legenda" :
                    baseLink = getResources().getStringArray(R.array.tarung_legenda_chapters_png)[position];
                    lastChapter = getResources().getStringArray(R.array.tarung_legenda_chapters_png).length;
                    extension = ".png";
                    break;
                case "Kris" :
                    baseLink = getResources().getStringArray(R.array.kris_chapters_jpg)[position];
                    lastChapter = getResources().getStringArray(R.array.kris_chapters_jpg).length;
                    extension = ".jpg";
                    break;
                case "Le Scenario":
                    baseLink = getResources().getStringArray(R.array.le_scenario_chapters_png)[position];
                    lastChapter = getResources().getStringArray(R.array.le_scenario_chapters_png).length;
                    extension = ".png";
                    isDoubleDigit = true;
                    break;
                case "Pacet":
                    baseLink = getResources().getStringArray(R.array.pacet_chapters_png)[position];
                    lastChapter = getResources().getStringArray(R.array.pacet_chapters_png).length;
                    extension = ".png";
                    isDoubleDigit = true;
                    break;
                default:
                    baseLink = "";
            }
            //Log.d("ASDF", "last chap " + lastChapter);
            links = new String[15];
            for(int i = 0 ; i < 15; i++){

                String prefix = "";

                if(isDoubleDigit && i+1 < 10){
                    prefix = "0";
                }

                links[i] = baseLink + prefix + (i+1) + extension;
            }

            String basePath = Environment.getExternalStorageDirectory() + "/" + Environment.getRootDirectory().getAbsolutePath()+"/.reon/"+title+"/"+(chapterNumber+1)+"/";
            File comicFolder = new File(basePath);
            if(comicFolder.exists()){
                downloadedImageLocations = new String[15];
                for(int i = 0 ; i < 15; i++){

                    downloadedImageLocations[i] = basePath+i+extension;
                }
            }else{
                downloadedImageLocations = new String[0];
            }


        }else{
            String[] placeHolderLinks = {
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_1.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_2.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_3.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_4.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_5.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_6.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_7.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_8.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_9.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_10.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_11.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_12.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_13.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_14.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_15.jpg",
                    "http://www.reoncomics.com/uploads/comic/320/155/kris1_16.jpg",
            };

            links = placeHolderLinks;

        }


        swapImageView.setImageDrawable(getDrawable(R.drawable.ic_swap_vert));

        SharedPreferences settingsPreference = this.getSharedPreferences("settings", Context.MODE_PRIVATE);
        String orientation = settingsPreference.getString("current_orientation", "HORIZONTAL");

        //Log.d("ADSF", "current orientation is " + orientation);

        if(orientation.equalsIgnoreCase("VERTICAL")){
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.comic_Read_act__comic_read_placeholder, ComicReaderVerticalFragment.newInstance(links, this, downloadedImageLocations));
            fragmentTransaction.commit();
        }else{
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.comic_Read_act__comic_read_placeholder, ComicReaderHorizontalFragment.newInstance(links, downloadedImageLocations, this));
            fragmentTransaction.commit();
            isVertical = !isVertical;
        }

        prepareToolbar();

    }
}

interface ChangeChapterListener {
    void moveToNextChapter();
    void moveToPreviousChapter();
    void toggleTopBar();
}