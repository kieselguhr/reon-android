package com.testing.wilbert.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class UserDatabase {

    public static final int USER_NOT_FOUND = -1;
    public static final int ERROR_OCCURED = -2;
    private static final int USER_CHANGE_SUCCESS = -3;
    public static final int DUPLICATION_ERROR = -4;
    ArrayList<CustomUser> users = new ArrayList<>();
    JSONArray usersJSONArray;
    Context context;

    SharedPreferences userSharedPreference;


    public String getUserArrayInString() {
        try {
            //Log.d("ASDF", "asdf array is" + usersJSONArray.toString(2));
            return usersJSONArray.toString(2);
        } catch (JSONException e) {
            e.printStackTrace();
            return "invalid JSON String";
        }
    }

    public UserDatabase(Context context) {

        userSharedPreference = context.getSharedPreferences("userDatabase", Context.MODE_PRIVATE);
//        clear();
        this.context = context;
        refreshUser();

    }

    public void clear() {
        SharedPreferences.Editor editor = userSharedPreference.edit();
        editor.putString("users", "[]");
        editor.commit();
        refreshUser();
    }


    public void logOut() {
        SharedPreferences userList = context.getSharedPreferences("users", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userList.edit();

        editor.putBoolean("auto_login", false);
        editor.commit();
    }

    public void addNewUser(CustomUser customUser) {
        users.add(customUser);

        JSONObject customUserJSON = new JSONObject();
        try {
            customUserJSON.put("username", customUser.getUsername());
            customUserJSON.put("password", customUser.getPassword());
            customUserJSON.put("email", customUser.getEmail());
            customUserJSON.put("gender", customUser.getGender());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        usersJSONArray.put(customUserJSON);
        updateUserPreference();
        refreshUser();
    }

    public String getActiveUsername() {
        SharedPreferences userList = context.getSharedPreferences("users", Context.MODE_PRIVATE);
        String activeUserName = userList.getString("active", "");
        return activeUserName;
    }

    public void setActiveUser(String username, boolean rememberUserPassword, boolean autoLogin, boolean isGoogle) {
        SharedPreferences userList = context.getSharedPreferences("users", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userList.edit();

        editor.putBoolean("password_remember", rememberUserPassword);
        editor.putBoolean("auto_login", autoLogin);
        editor.putString("active", username);
        editor.putBoolean("is_google", isGoogle);
        editor.commit();
    }

    public boolean isPasswordRemembered() {
        SharedPreferences userList = context.getSharedPreferences("users", Context.MODE_PRIVATE);
        return userList.getBoolean("password_remember", false);
    }

    public boolean isAutoLogin() {
        SharedPreferences userList = context.getSharedPreferences("users", Context.MODE_PRIVATE);
        return userList.getBoolean("auto_login", false);
    }

    public boolean isGoogle() {
        SharedPreferences userList = context.getSharedPreferences("users", Context.MODE_PRIVATE);
        return userList.getBoolean("is_google", false);
    }

    public CustomUser getActiveUser() {
        int userID = findUserID(getActiveUsername());
        if(userID < 0){
            return null;
        }
        try {
            JSONObject activeUserJSON = usersJSONArray.getJSONObject(userID);
            CustomUser activeUser = new CustomUser(activeUserJSON.getString("username"), activeUserJSON.getString("password"), activeUserJSON.getString("email"), activeUserJSON.getString("gender"));
            activeUser.setGenres(getUserGenres(getActiveUsername()));

            return activeUser;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void initializeGenreToUser(String username, ArrayList<String> genres) {
        try {
            int userID = findUserID(username);

            JSONArray genresJSON = new JSONArray();

            for (String g : genres) {
                genresJSON.put(g);
            }

            if (usersJSONArray.getJSONObject(userID).has("genres")) {

                if (usersJSONArray.getJSONObject(userID).getJSONArray("genres").length() < 1) {
                    usersJSONArray.getJSONObject(userID).put("genres", genresJSON);
                } else {
                    JSONArray newHistoryNode = new JSONArray();
                    newHistoryNode.put(usersJSONArray.getJSONObject(userID).getJSONArray("genres").get(0));

                    usersJSONArray.getJSONObject(userID).put("genres", newHistoryNode);
                }


//                usersJSONArray.getJSONObject(userID).getJSONArray("history_nodes").put(historyNodeJSON);


            } else {
                usersJSONArray.getJSONObject(userID).put("genres", genresJSON);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        updateUserPreference();

    }

    public static ArrayList<String> covertJSONArraytoArrayList(JSONArray jsonArray) {
        ArrayList<String> genreArray = new ArrayList<>();
        try {

            for (int i = 0; i < jsonArray.length(); i++) {

                String iGenre = jsonArray.getString(i);

                genreArray.add(iGenre);
            }

            return genreArray;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<String> getUserGenres(String username) {

        int userID = findUserID(username);

        try {

//            //Log.d("ASDF",usersJSONArray.getJSONObject(userID).getJSONArray("history_nodes").toString(2));

            ArrayList<String> genreJSONArray = new ArrayList<>();

            if (usersJSONArray.getJSONObject(userID).has("genres")) {
                JSONArray historyNodesJSON = usersJSONArray.getJSONObject(userID).getJSONArray("genres");
                for (int i = 0; i < historyNodesJSON.length(); i++) {

                    String iGenre = historyNodesJSON.getString(i);

                    genreJSONArray.add(iGenre);
                }
            }

            return genreJSONArray;

        } catch (JSONException e) {
            //Log.d("ASDF", "get History node parse error");
            e.printStackTrace();
            return new ArrayList<>();

        }
    }

    public boolean changePassword(String oldPassword, String newPassword){
        if(getActiveUser().getPassword().equals(oldPassword)){
            try {

                int userID = findUserID(getActiveUsername());
                JSONObject editedUserJSON = usersJSONArray.getJSONObject(userID);
//            //Log.d("ASDF to string", genresJSON.toString());
                editedUserJSON.put("password", newPassword);

                usersJSONArray.put(userID, editedUserJSON);

                updateUserPreference();
                return true;

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }else{
            return false;
        }

    }


    public int findUserID(String username) {
        try {
            for (int i = 0; i < usersJSONArray.length(); i++) {

                String iUsername = usersJSONArray.getJSONObject(i).getString("username");

                if (iUsername.equals(username) ) {
                    return i;
                }

            }
            return USER_NOT_FOUND;
        } catch (JSONException e) {
            e.printStackTrace();
            return ERROR_OCCURED;
        }
    }

    public int findUserIDbyEmail(String email) {
        try {
            for (int i = 0; i < usersJSONArray.length(); i++) {

                String iEmail = usersJSONArray.getJSONObject(i).getString("email");

                if (iEmail.equals(email)) {
                    return i;
                }

            }
            return USER_NOT_FOUND;
        } catch (JSONException e) {
            e.printStackTrace();
            return ERROR_OCCURED;
        }
    }

    public boolean editPassword(int userID, String oldPassword, String newPassword){
        try{
            JSONObject newUserJSON = usersJSONArray.getJSONObject(userID);

            if(oldPassword.equals(newUserJSON.getString("password"))){
                newUserJSON.put("password", newPassword);
                usersJSONArray.put(userID, newUserJSON);
                updateUserPreference();
                return true;
            }else{
                return false;
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return false;
    }

    public int editUser(int userID, String email, String username, String password, JSONArray genresJSON) {
        try {

            JSONObject newUserJSON = usersJSONArray.getJSONObject(userID);
//            //Log.d("ASDF to string", genresJSON.toString());

            if (username != null) {
                newUserJSON.put("username", username);
            }

            if (email != null) {
                newUserJSON.put("email", email);
            }

            if (genresJSON != null) {
                usersJSONArray.getJSONObject(userID).put("genres", genresJSON);
            }

            usersJSONArray.put(userID, newUserJSON);

            updateUserPreference();
            return USER_CHANGE_SUCCESS;

        } catch (JSONException e) {
            e.printStackTrace();
            return ERROR_OCCURED;
        }

    }

    public String authenticateGoogleLogin(String email){
        try {
            for (int i = 0; i < usersJSONArray.length(); i++) {

                String iUsername = usersJSONArray.getJSONObject(i).getString("username");
                String iEmail = usersJSONArray.getJSONObject(i).getString("email");

                if (iEmail.equals(email)) {
                    return iUsername;
                }

            }
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String authenticateUser(String username, String password) {
        try {
            for (int i = 0; i < usersJSONArray.length(); i++) {

                String iUsername = usersJSONArray.getJSONObject(i).getString("username");
                String iEmail = usersJSONArray.getJSONObject(i).getString("email");
                String iPassword = usersJSONArray.getJSONObject(i).getString("password");

                if (iUsername.equals(username) || iEmail.equals(username)) {
                    if (iPassword.equals(password)) {
                        return iUsername;
                    } else {
                        return null;
                    }
                }

            }
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void updateUserPreference() {

        SharedPreferences.Editor editor = userSharedPreference.edit();
        editor.putString("users", usersJSONArray.toString());
        editor.commit();

    }

    public void refreshUser() {
        SharedPreferences userSharedPreference = context.getSharedPreferences("userDatabase", Context.MODE_PRIVATE);

        try {
            usersJSONArray = new JSONArray("[]");

            String userSharedPreferenceString = "[]";

            if (userSharedPreference.getString("users", null) == null) {

                SharedPreferences.Editor edit = userSharedPreference.edit();
                edit.putString("users", "[]");
                edit.commit();

            } else {
                userSharedPreferenceString = userSharedPreference.getString("users", null);
            }
            usersJSONArray = new JSONArray(userSharedPreferenceString);


            //Log.d("ASDF", "full array is " + usersJSONArray);

            for (int i = 0; i < usersJSONArray.length(); i++) {
                JSONObject user = usersJSONArray.getJSONObject(i);

                users.add(new CustomUser(user.getString("username"), user.getString("password"), user.getString("email"), user.getString("gender")));

            }

        } catch (JSONException e) {
            e.printStackTrace();


        }
    }

    public int[] getFavouriteComicsId(String username) {

        int userID = findUserID(username);
        if (userID == USER_NOT_FOUND) {
            return new int[0];
        }

        try {
            if (usersJSONArray.getJSONObject(userID).has("favourites")) {

                JSONArray userFavourites = usersJSONArray.getJSONObject(userID).getJSONArray("favourites");

                int favouritesIDArray[] = new int[userFavourites.length()];

                for (int i = 0; i < userFavourites.length(); i++) {

                    favouritesIDArray[i] = userFavourites.getInt(i);

                }

                return favouritesIDArray;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new int[0];

    }

    public int[] getHistoryComicsId(String username) {

        int userID = findUserID(username);
        if (userID == USER_NOT_FOUND) {
            return new int[0];
        }

        try {
            if (usersJSONArray.getJSONObject(userID).has("history")) {

                JSONArray userHistory = usersJSONArray.getJSONObject(userID).getJSONArray("history");

                int historyIDArray[] = new int[userHistory.length()];

                for (int i = 0; i < userHistory.length(); i++) {

                    historyIDArray[i] = userHistory.getInt(i);

                }

                return historyIDArray;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new int[0];

    }


    public boolean isComicFavourite(String username, int comicId) {
        try {
            int userID = findUserID(username);
            if (userID == USER_NOT_FOUND) {
                return false;
            }

            if (usersJSONArray.getJSONObject(userID).has("favourites")) {

                JSONArray userFavourites = usersJSONArray.getJSONObject(userID).getJSONArray("favourites");

                for (int i = 0; i < userFavourites.length(); i++) {

                    int iFavouritesId = userFavourites.getInt(i);
                    if (iFavouritesId == comicId) {
                        return true;
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean removeFavouriteFromUser(String username, int comicId) {
        try {
            int userID = findUserID(username);
            if (userID == USER_NOT_FOUND) {
                return false;
            }

            if (usersJSONArray.getJSONObject(userID).has("favourites")) {

                if (usersJSONArray.getJSONObject(userID).getJSONArray("favourites").length() < 1) {
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(comicId);
                    usersJSONArray.getJSONObject(userID).put("favourites", jsonArray);
                } else {

                    JSONArray userFavourites = usersJSONArray.getJSONObject(userID).getJSONArray("favourites");

                    for (int i = 0; i < userFavourites.length(); i++) {

                        int iFavouritesId = userFavourites.getInt(i);
                        if (iFavouritesId == comicId) {
                            userFavourites.remove(i);
                            updateUserPreference();
                            return true;
                        }
                    }

                }

            } else {
                JSONArray userFavourites = new JSONArray();
                userFavourites.put(comicId);
                usersJSONArray.getJSONObject(userID).put("favourites", userFavourites);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    public int addHistoryToUser(String username, int comicId){
        try {
            int userID = findUserID(username);
            if (userID == USER_NOT_FOUND) {
                return USER_NOT_FOUND;
            }

            if (usersJSONArray.getJSONObject(userID).has("history")) {

                if (usersJSONArray.getJSONObject(userID).getJSONArray("history").length() < 1) {
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(comicId);
                    usersJSONArray.getJSONObject(userID).put("history", jsonArray);
                } else {

                    JSONArray history = usersJSONArray.getJSONObject(userID).getJSONArray("history");

                    for (int i = 0; i < history.length(); i++) {

                        int iFavouritesId = history.getInt(i);
                        if (iFavouritesId == comicId) {
                            //Log.d("ASDF", "IT IS ALREADY IN HISTORY !");
                            return DUPLICATION_ERROR;
                        }
                    }

                    history.put(comicId);

                }

            } else {
                JSONArray history = new JSONArray();
                history.put(comicId);
                usersJSONArray.getJSONObject(userID).put("history", history);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return ERROR_OCCURED;
        }

        updateUserPreference();
        return USER_CHANGE_SUCCESS;
    }

    public int addFavouritesToUser(String username, int comicId) {
        try {
            int userID = findUserID(username);
            if (userID == USER_NOT_FOUND) {
                return USER_NOT_FOUND;
            }

            if (usersJSONArray.getJSONObject(userID).has("favourites")) {

                if (usersJSONArray.getJSONObject(userID).getJSONArray("favourites").length() < 1) {
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(comicId);
                    usersJSONArray.getJSONObject(userID).put("favourites", jsonArray);
                } else {

                    JSONArray userFavourites = usersJSONArray.getJSONObject(userID).getJSONArray("favourites");

                    for (int i = 0; i < userFavourites.length(); i++) {

                        int iFavouritesId = userFavourites.getInt(i);
                        if (iFavouritesId == comicId) {
                            //Log.d("ASDF", "IT IS ALREADY IN FAVOURITE !");
                            return DUPLICATION_ERROR;
                        }
                    }

                    userFavourites.put(comicId);

                }

            } else {
                JSONArray userFavourites = new JSONArray();
                userFavourites.put(comicId);
                usersJSONArray.getJSONObject(userID).put("favourites", userFavourites);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return ERROR_OCCURED;
        }

        updateUserPreference();
        return USER_CHANGE_SUCCESS;

    }


}

class CustomUser {

    private String username;
    private String password;
    private String email;
    private String gender;

    private ArrayList<String> genres = new ArrayList<>();

    private ArrayList<Comic> favourites = new ArrayList<>();

    public ArrayList<Comic> getFavourites() {
        return favourites;
    }

    public void setFavourites(ArrayList<Comic> favourites) {
        this.favourites = favourites;
    }

    public CustomUser(String username, String password, String email, String gender) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.gender = gender;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
