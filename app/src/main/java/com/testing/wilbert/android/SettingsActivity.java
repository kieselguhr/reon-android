package com.testing.wilbert.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.settings_act__read_direction_spinner)
    Spinner readDirection;

    @OnClick(R.id.settings_act__version_view)
    void showVersion(){

        String content = "App Version : 0.0.1";

        final CustomAlertDialog customAlertDialog = CustomAlertDialog.newInstance("Version", content, "Ok", "", CustomAlertDialog.DEFAULT_LISTENER);
        customAlertDialog.show(getFragmentManager(), "ASDF");

    }

    @OnClick(R.id.settings_act__disclaimer_view)
    void showDisclaimer(){

        String content = "- All comics belong to their respective copyright owners.\n" +
                "- Reon Comics have the right to change content data without  user permission.";

        final CustomAlertDialog customAlertDialog = CustomAlertDialog.newInstance("Disclaimer", content, "Ok", "", CustomAlertDialog.DEFAULT_LISTENER);
        customAlertDialog.show(getFragmentManager(), "ASDF");

    }

    @OnClick(R.id.settings_act__about_us_view)
    void showAboutUs(){
        String content = "Reon Comics is series comic that published regularly and mobile-optimized. Reon Comics has various genres like drama, fantasy, sci-fi, comedy, action, romance, etc.";

        final CustomAlertDialog customAlertDialog = CustomAlertDialog.newInstance("About Us", content, "Ok", "", CustomAlertDialog.DEFAULT_LISTENER);
        customAlertDialog.show(getFragmentManager(), "ASDF");
    }

    @BindView(R.id.settings_act__cellular_download_switch)
    Switch cellularDownloadSwitch;

    @OnClick(R.id.settings_act__cellular_download_switch)
    void toggleCellularDownlaod(){
        SharedPreferences.Editor editor = settingsPreference.edit();

        editor.putBoolean("cellular_download", cellularDownloadSwitch.isChecked());
        editor.commit();
    }

    SharedPreferences settingsPreference;

    public boolean onOptionsItemSelected(MenuItem item){
       this.finish();
       return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setTitle("Settings");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        settingsPreference = this.getSharedPreferences("settings", Context.MODE_PRIVATE);

        cellularDownloadSwitch.setChecked(settingsPreference.getBoolean("cellular_download", false));

        final String selection[] = new String[]{"Horizontal", "Vertical"};

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, R.layout.row_dropdown_item, selection );

        readDirection.setAdapter(spinnerAdapter);
        readDirection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = settingsPreference.edit();

                editor.putString("reader_orientation", selection[i].toUpperCase());
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        String orientation = settingsPreference.getString("reader_orientation", "HORIZONTAL");

        if(orientation.equalsIgnoreCase("VERTICAL")){
            readDirection.setSelection(1);
        }


    }
}
