package com.testing.wilbert.android;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class MoreMenuFragment extends Fragment {


    public MoreMenuFragment() {
        // Required empty public constructor
    }

    public static int MY_ACCOUNT_ACTIVITY_CODE = 1;

    @OnClick(R.id.more_menu_frag__shop_view)
    void openShop(){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.tokopedia.com/reoncomics"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.more_menu_frag__my_account_view)
    void callMyAccount(){
        Intent intent = new Intent(getContext(), MyAccountActivity.class);
        getActivity().startActivityForResult(intent, MY_ACCOUNT_ACTIVITY_CODE);
    }

    @OnClick(R.id.more_menu_frag__settings_view)
    void callSettings(){
        Intent intent = new Intent(getContext(), SettingsActivity.class);
        getActivity().startActivityForResult(intent, MY_ACCOUNT_ACTIVITY_CODE);
    }

    private CustomBackPressedListener backPressedListener = null;

    @OnClick(R.id.more_menu_frag__my_downloads_view)
    void callDownloads(){
//        Intent intent = new Intent(getContext(), DownloadedChaptersActivity.class);
//        getActivity().startActivityForResult(intent, MY_ACCOUNT_ACTIVITY_CODE);
        final DownloadedTitlesFragment downloadedTitlesFragment = new DownloadedTitlesFragment();

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.more_menu_frag__root_view, downloadedTitlesFragment);
        ft.commit();

        backPressedListener = new CustomBackPressedListener() {
            @Override
            public void onBackButtonPressed() {
                downloadedTitlesFragment.backListener();
//                backPressedListener = null;
            }
        };

        if(getActivity() instanceof MainActivity){
            ((MainActivity) getActivity()).setCustomBackPressedListener(backPressedListener);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

    }

    public CustomBackPressedListener getBackPressedListener() {
        return backPressedListener;
    }
}
