package com.testing.wilbert.android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity {

    private final static int RC_SIGN_IN = 212;
    private final static int REGISTER_ACTIVITY_FORMS = 650;

    @OnClick(R.id.sign_in_act__email_sign_in_btn)
    void callEmailSignIn(){
        Intent intent = new Intent(this, EmailSignInActivity.class);
        startActivity(intent);
    }

//    @OnClick(R.id.sign_in_act__google_sign_in_btn)
//    void callGoogleSignIn(){
//        callToastUnderConstruction();
//    }

    @OnClick(R.id.sign_in_act__facebook_sign_in_btn)
    void callFacebookSignIn(){
        callToastUnderConstruction();
    }

    void callToastUnderConstruction(){
        Toast.makeText(this, "This feature is under construction", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.sign_in_act__google_sign_in_btn)
    void googleSignIn(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        ButterKnife.bind(this);

        UserDatabase userDatabase = new UserDatabase(this);
//        userDatabase.setActiveUser("");
//        userDatabase.clear();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .build();
        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleSignInClient.signOut();


        if(userDatabase.isAutoLogin() && !userDatabase.getActiveUsername().equals("") && !userDatabase.isGoogle()){
            Intent intent = new Intent ( this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        Objects.requireNonNull(getSupportActionBar()).hide();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_ACTIVITY_FORMS){
            if(resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();

//                //Log.d("ASDF profile", "username = "+  extras.getString("username"));
//                //Log.d("ASDF profile", "email = "+  extras.getString("email"));
//                //Log.d("ASDF profile", "password = "+  extras.getString("password"));
//
//                //Log.d("ASDF profile", "gender = " + extras.getString("gender"));
//
//                //Log.d("ASDF profile", "genres = " + extras.getString("genres"));

                UserDatabase userDatabase = new UserDatabase(this);
                try {
                    JSONArray genresJSON = new JSONArray(extras.getString("genres"));
                    ArrayList<String> genres = new ArrayList<>();

                    for(int i = 0 ;  i < genresJSON.length() ; i++){
                        genres.add(genresJSON.getString(i));
                    }

                    userDatabase.addNewUser(new CustomUser(extras.getString("username"), extras.getString("password"), extras.getString("email"), extras.getString("gender")));
                    userDatabase.initializeGenreToUser(extras.getString("username"), genres);

                    userDatabase.setActiveUser(extras.getString("username"), false, false, false);

                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);

//                Toast.makeText(this, "Welcome new user", Toast.LENGTH_SHORT).show();
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }

        if(requestCode == RC_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);

                // Signed in successfully, show authenticated UI.
                Log.d("ASDF", "login of "  +account.getEmail());
                UserDatabase userDatabase = new UserDatabase(this);
                if(userDatabase.findUserIDbyEmail(account.getEmail()) != UserDatabase.USER_NOT_FOUND){
                    Intent intent = new Intent(this, MainActivity.class);
                    userDatabase.setActiveUser(userDatabase.authenticateGoogleLogin(account.getEmail()), true, true, true);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(this, RegisterGeneralDataActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("username", account.getDisplayName());
                    extras.putString("email", account.getEmail());
                    intent.putExtras(extras);
                    //userDatabase.addNewUser(new CustomUser(account.getDisplayName(), "", account.getEmail(), "boy"));
//                    userDatabase.setActiveUser(account.getDisplayName(), true, true, true);
                    startActivityForResult(intent, REGISTER_ACTIVITY_FORMS);
//                    finish();
                }
            } catch (ApiException e) {
                e.printStackTrace();
                e.getCause();
                Toast.makeText(this, "An error has occured", Toast.LENGTH_SHORT).show();
                // The ApiException status code indicates the detailed failure reason.
                // Please refer to the GoogleSignInStatusCodes class reference for more information.
                Log.d("ASDF", "login of fail" );

            }
        }


    }
}
