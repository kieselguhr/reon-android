package com.testing.wilbert.android;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class DiscoverFragment extends Fragment {

    @BindView(R.id.discover_frag__view_pager)
    ViewPager viewPager;

    @BindView(R.id.discover_frag__tab_layout)
    TabLayout tabLayout;

    String genres[] = new String[]{
            "Sport",
            "Comedy",
            "Romance",
            "Daily Life",
            "Fantasy",
            "Action",
            "Sci-Fi",
            "Mystery",
            "Thriller",
            "Horror",
            "Drama",
            "Psychological",
            "Supernatural",
            "Other"
    };

    public DiscoverFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_discover, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < genres.length; i++) {
            changeToCustomTahLayout(i);
        }


    }

    ComicModel comicModel;
    ViewPagerAdapter adapter;

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getFragmentManager());
        viewPager.setOffscreenPageLimit(3);
        comicModel = new ComicModel(getContext());

        for (String g : genres) {

            addFragmentToViewPager(g);

        }

        viewPager.setAdapter(adapter);

    }

    private void addFragmentToViewPager(String genre) {

        adapter.addFragment(GenreDetailFragment.newInstance(genre), genre);

    }

    private void changeToCustomTahLayout(int index) {

        TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.search_toolbar_view_pager_node, null);

        textView.setText(genres[index]);

        tabLayout.getTabAt(index).setCustomView(textView);
    }


}
