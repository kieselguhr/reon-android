package com.testing.wilbert.android;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadedChaptersActivity extends AppCompatActivity {

    @BindView(R.id.downloaded_chapters_act__title)
    TextView titleText;

    @BindView(R.id.downloaded_chapters_act__comic_count)
    TextView comicCountText;

    @BindView(R.id.downloaded_chapters_act__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.downloaded_chapters_act__empty_text)
    TextView emptyView;

    ArrayList<Comic> comics;

    @BindView(R.id.custom_toolbar)
    LinearLayout toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloaded_chapters);
        ButterKnife.bind(this);

        getSupportActionBar().hide();

        ComicModel comicModel = new ComicModel(this);

        Bundle params = getIntent().getExtras();

        titleText.setText("Downloaded");

        String path = Environment.getExternalStorageDirectory() + "/" + Environment.getRootDirectory().getAbsolutePath()+"/.reon/";

        comics = new ArrayList<>();

        File f = new File(path);
        File[] files = f.listFiles();

        if (files != null){
            for (File inFile : files) {
                if (inFile.isDirectory()) {
                    //Log.d("ASDF", inFile.getName());
                    comics.add(comicModel.getComicById(comicModel.getComicById(inFile.getName())));
                }
            }
        }


        if(comics.size() > 0){
            emptyView.setVisibility(View.GONE);
        }

//        comics = ComicModel.convertJSONArrayToComicArrayList(new JSONArray(params.getString("comics")));
        comicCountText.setText(comics.size() + " Comics");
//
        ImageStripDetailAdapter imageStripDetailAdapter = new ImageStripDetailAdapter(comics);
//
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
//
        recyclerView.setAdapter(imageStripDetailAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);



        ImageView toolbarSearchButton = toolbar.findViewById(R.id.main_toolbar__search_btn);
        toolbarSearchButton.setClickable(true);
        toolbarSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DownloadedChaptersActivity.this, SearchActivity.class);

                Bundle extras = new Bundle();
//                extras.putString("");

                startActivity(intent);
            }
        });

        ImageView backButton = toolbar.findViewById(R.id.main_toolbar__back_btn);
        backButton.setVisibility(View.VISIBLE);
        backButton.setClickable(true);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadedChaptersActivity.this.finish();
            }
        });

    }
}
