package com.testing.wilbert.android;


import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class DownloadedTitlesFragment extends Fragment {

    @BindView(R.id.downloaded_titles_frag__title)
    TextView titleText;

    @BindView(R.id.downloaded_titles_frag__comic_count)
    TextView comicCountText;

    @BindView(R.id.downloaded_titles_frag__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.downloaded_titles_frag__change_order)
    TextView changeOrder;

    @BindView(R.id.downloaded_titles_frag__empty_text)
    TextView emptyView;

    ArrayList<Comic> comics;
    private String title;

    ImageStripDetailAdapter imageStripDetailAdapter;

    @OnClick(R.id.downloaded_titles_frag__back)
    void back(){
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.downloaded_titles_frag__root_view, new MoreMenuFragment());
        ft.commit();
    }
    public void backListener(){
//        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        ft.replace(R.id.image_strip_detail_frag__root_view, new HomeFragment());
//        ft.commit();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.downloaded_titles_frag__root_view, new MoreMenuFragment());
        ft.commit();
    }
    @OnClick(R.id.downloaded_titles_frag__change_order)
    void changeOrder() {

        String status = changeOrder.getText().toString();

        if(status.contains("Updated")){

            changeOrder.setText("Sort By : Alphabetical");

            comics = ComicModel.sortComicAlhpabetically(comics);

            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();


        }else if(status.contains("Alphabetical")){

            changeOrder.setText("Sort By : Updated");
            imageStripDetailAdapter.notifyDataSetChanged();

        }

    }

    public static DownloadedTitlesFragment newInstance(String title){
        DownloadedTitlesFragment downloadedTitlesFragment = new DownloadedTitlesFragment();
        downloadedTitlesFragment.setTitle(title);
        return downloadedTitlesFragment;
    }

    public DownloadedTitlesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_downloaded_titles, container, false);
    }

    UserDatabase userDatabase;
    ComicModel comicModel;

    private void getDownloadedTitles(){
        String path = Environment.getExternalStorageDirectory() + "/" + Environment.getRootDirectory().getAbsolutePath()+"/.reon/";

        comics = new ArrayList<>();

        File f = new File(path);
        File[] files = f.listFiles();

        if (files != null){
            for (File inFile : files) {
                if (inFile.isDirectory()) {
                    //Log.d("ASDF", inFile.getName());
                    comics.add(comicModel.getComicById(comicModel.getComicById(inFile.getName())));
                }
            }
        }


        ImageStripDetailAdapter imageStripDetailAdapter = new ImageStripDetailAdapter(comics, DownloadedComicDetailActivity.class);


        if(comics.size() > 0){
            emptyView.setVisibility(View.GONE);
        }
        comicCountText.setText(comics.size() + " Comics");
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(imageStripDetailAdapter);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        titleText.setText(title);

        userDatabase = new UserDatabase(getContext());
        comicModel = new ComicModel(getContext());

        imageStripDetailAdapter = new ImageStripDetailAdapter(comics, DownloadedComicDetailActivity.class);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(imageStripDetailAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

        titleText.setText("Downloaded");



//        comics = ComicModel.convertJSONArrayToComicArrayList(new JSONArray(params.getString("comics")));

//
//
//
        getDownloadedTitles();



    }

    public void setComics(ArrayList<Comic> comics) {
        this.comics = comics;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
