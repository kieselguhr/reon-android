package com.testing.wilbert.android;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryComicFragment extends Fragment {


    @BindView(R.id.history_comic_frag__title)
    TextView titleText;

    @BindView(R.id.history_comic_frag__comic_count)
    TextView comicCountText;

    @BindView(R.id.history_comic_frag__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.history_comic_frag__empty_text)
    TextView emptyView;

    ArrayList<Comic> comics;

    @BindView(R.id.history_comic_frag__change_order)
    TextView changeOrder;

    @OnClick(R.id.history_comic_frag__change_order)
    void changeOrder() {

        String status = changeOrder.getText().toString();

        if(status.contains("Updated")){

            changeOrder.setText("Sort By : Alphabetical");

            comics = ComicModel.sortComicAlhpabetically(comics);

            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();


        }else if(status.contains("Alphabetical")){

            changeOrder.setText("Sort By : Updated");
            comics = comicModel.getComicsByIds(userDatabase.getFavouriteComicsId(userDatabase.getActiveUsername()));
            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();

        }

    }

    public HistoryComicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_comic, container, false);
    }

    ComicModel comicModel;
    UserDatabase userDatabase;
    ImageStripDetailAdapter imageStripDetailAdapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        titleText.setText("Recent");

        comicModel = new ComicModel(getContext());
        userDatabase = new UserDatabase(getContext());

        comics = comicModel.getComicsByIds(userDatabase.getHistoryComicsId(userDatabase.getActiveUsername()));

        if(comics.size() > 0){
            emptyView.setVisibility(View.GONE);
        }

        comicCountText.setText(comics.size() + " Comics");

        imageStripDetailAdapter = new ImageStripDetailAdapter(comics);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(imageStripDetailAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);


    }
}
