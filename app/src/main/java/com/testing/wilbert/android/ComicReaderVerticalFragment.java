package com.testing.wilbert.android;


import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComicReaderVerticalFragment extends Fragment {

    @BindView(R.id.comic_read_vertical_frag__recycler_view)
    RecyclerView recyclerView;

    ComicReaderVerticalAdapter comicReaderVerticalAdapter;

    private boolean showOnlyDownloaded;

    public ComicReaderVerticalFragment() {
        // Required empty public constructor

    }

    private String links[];
    private String downloadedImageLocations[];

    private ChangeChapterListener changeChapterListener;

    public ChangeChapterListener getChangeChapterListener() {
        return changeChapterListener;
    }

    public void setChangeChapterListener(ChangeChapterListener changeChapterListener) {
        this.changeChapterListener = changeChapterListener;
    }

    public String[] getLinks() {
        return links;
    }

    public void setLinks(String[] links) {
        this.links = links;
    }

    public static ComicReaderVerticalFragment newInstance(String[] links, ChangeChapterListener changeChapterListener, String downloadedImageLocations[]){
        ComicReaderVerticalFragment comicReaderVerticalFragment = new ComicReaderVerticalFragment();
        comicReaderVerticalFragment.setLinks(links);
        comicReaderVerticalFragment.setChangeChapterListener(changeChapterListener);
        comicReaderVerticalFragment.setDownloadedImageLocations(downloadedImageLocations);

        comicReaderVerticalFragment.setShowOnlyDownloaded(false);

        return comicReaderVerticalFragment;
    }
    public static ComicReaderVerticalFragment newInstance(ChangeChapterListener changeChapterListener, String downloadedImageLocations[] ){
        ComicReaderVerticalFragment comicReaderVerticalFragment = new ComicReaderVerticalFragment();
        comicReaderVerticalFragment.setChangeChapterListener(changeChapterListener);
        comicReaderVerticalFragment.setDownloadedImageLocations(downloadedImageLocations);

        comicReaderVerticalFragment.setShowOnlyDownloaded(true);
        return comicReaderVerticalFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comic_reader_vertical, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

//        String[] links = {
//                "http://www.reoncomics.com/uploads/comic/320/155/kris1_1.jpg"
//        };


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false);

        if(showOnlyDownloaded){
            comicReaderVerticalAdapter = new ComicReaderVerticalAdapter(downloadedImageLocations, changeChapterListener);
        }else{
            comicReaderVerticalAdapter = new ComicReaderVerticalAdapter(links, downloadedImageLocations, changeChapterListener);
        }

        recyclerView.setAdapter(comicReaderVerticalAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

        final boolean[] scrollToNextReady = {true};
        final boolean[] scrollToPreviousReady = {false};


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {

                    GridLayoutManager gridLayoutManager1 = (GridLayoutManager) recyclerView.getLayoutManager();

                    int length;
                    if(links == null){
                        length = downloadedImageLocations.length;
                    }else{
                        length = links.length;
                    }

                    if(gridLayoutManager1.findLastVisibleItemPosition() >= length - 1 && scrollToNextReady[0]){
                        changeChapterListener.moveToNextChapter();
                        scrollToNextReady[0] = false;
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                scrollToNextReady[0] = true;
                            }
                        }, 1000);
                    }

                }

                if(!recyclerView.canScrollVertically(-1)){
                    GridLayoutManager gridLayoutManager1 = (GridLayoutManager) recyclerView.getLayoutManager();

                    if(scrollToPreviousReady[0]){
                        //Log.d("ASDF", "First");
                        changeChapterListener.moveToPreviousChapter();
                        scrollToPreviousReady[0] = false;
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                scrollToPreviousReady[0] = true;
                            }
                        }, 1000);
                    }

                }

            }
        });
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                scrollToPreviousReady[0] = true;
            }
        }, 2000);

    }

    public void setDownloadedImageLocations(String[] downloadedImageLocations) {
        this.downloadedImageLocations = downloadedImageLocations;
    }

    public void setShowOnlyDownloaded(boolean showOnlyDownloaded) {
        this.showOnlyDownloaded = showOnlyDownloaded;
    }
}

class ComicReaderVerticalAdapter extends RecyclerView.Adapter<ComicReaderRowViewHolder> {

    private String[] imageLinks;
    private String[] downloadedImageLocations;

    Context mContext;
    LayoutInflater mLayoutInflater;
    ChangeChapterListener changeChapterListener;
    boolean showOnlyDownloaded = false;

    public ComicReaderVerticalAdapter(String[] imageLinks, String[] downloadedImageLocations, ChangeChapterListener changeChapterListener) {
        this.imageLinks = imageLinks;
        this.downloadedImageLocations = downloadedImageLocations;
        this.changeChapterListener = changeChapterListener;
        //Log.d("ASDF", "normal initializer called");
    }

    public ComicReaderVerticalAdapter(String[] downloadedImageLocations, ChangeChapterListener changeChapterListener) {
        this.downloadedImageLocations = downloadedImageLocations;
        this.changeChapterListener = changeChapterListener;
        this.showOnlyDownloaded = true;
        //Log.d("ASDF", " verticla lnegth is " + downloadedImageLocations.length);
    }


    @NonNull
    @Override
    public ComicReaderRowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.row_comic_reader, parent, false);
        return new ComicReaderRowViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull ComicReaderRowViewHolder holder, final int position) {

//        //Log.d("ASDF path is", "downlaoded image length is " + downloadedImageLocations.length);

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.setColorFilter(mContext.getResources().getColor(R.color.white), PorterDuff.Mode.SRC );
        circularProgressDrawable.start();

        Drawable errorDrawable = mContext.getResources().getDrawable(R.drawable.ic_broken_image);
        errorDrawable.setTint(mContext.getResources().getColor(R.color.white));
        errorDrawable.setBounds(0,0,25,25);

        if(showOnlyDownloaded){

            File file = new File(downloadedImageLocations[position]);
                Glide.with(mContext).load(file)
                        .thumbnail(0.5f)
                        .apply(new RequestOptions()
                                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(circularProgressDrawable).error(errorDrawable) )
                        .into(holder.image);

//                //Log.d("ASDF", " file exits");

        }else{
            Glide.with(mContext).load(imageLinks[position])
                    .thumbnail(0.5f)
                    .apply(new RequestOptions()
                            .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.RESOURCE).placeholder(circularProgressDrawable).error(errorDrawable))
                    .into(holder.image);
        }





        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeChapterListener.toggleTopBar();
            }
        });

    }

    @Override
    public int getItemCount() {

        if(imageLinks != null){
            return imageLinks.length;
        }
        return downloadedImageLocations.length;
    }
}

class ComicReaderRowViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.comic_reader_row__image)
    ImageView image;

    Context mContext;

    public ComicReaderRowViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}
