package com.testing.wilbert.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmailSignInActivity extends AppCompatActivity {

    private final static int REGISTER_ACTIVITY_FORMS = 123;

    @BindView(R.id.email_sign_in_act__username_edit)
    EditText usernameEdit;

    @BindView(R.id.email_sign_in_act__password_edit)
    EditText passwordEdit;

    @BindView(R.id.email_sign_in_act__remember_me_check_box)
    CheckBox rememberMeCheck;

    @BindView(R.id.email_sign_in_act__auto_login_check_box)
    CheckBox autoLoginCheck;

    @OnClick(R.id.email_sign_in_act__forget_pwd)
    void goToForgotPassword(){
        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.email_sign_in_act__register_btn)
    void callRegister(){
        Intent intent = new Intent(this, RegisterGeneralDataActivity.class);
        startActivityForResult(intent, REGISTER_ACTIVITY_FORMS);
    }

    @OnClick(R.id.email_sign_in_act__sign_in_btn)
    void signIn(){
        UserDatabase userDatabase = new UserDatabase(this);
        String authenticationResult = userDatabase.authenticateUser(usernameEdit.getText().toString(), passwordEdit.getText().toString());
        if(authenticationResult != null){
            Intent intent = new Intent(this, MainActivity.class);
            userDatabase.setActiveUser(authenticationResult, rememberMeCheck.isChecked(), autoLoginCheck.isChecked(), false);
            startActivity(intent);

            finish();
        }else{
            Toast.makeText(this, "Invalid Credential", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Sign In");
        setContentView(R.layout.activity_email_sign_in);

        ButterKnife.bind(this);

        UserDatabase userDatabase = new UserDatabase(this);

        if(userDatabase.getActiveUser() != null){
            usernameEdit.setText(userDatabase.getActiveUsername());

            if(userDatabase.isPasswordRemembered()){
                passwordEdit.setText(userDatabase.getActiveUser().getPassword());
                rememberMeCheck.setChecked(true);
            }

        }



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        if(requestCode == REGISTER_ACTIVITY_FORMS){
            if(resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();

//                //Log.d("ASDF profile", "username = "+  extras.getString("username"));
//                //Log.d("ASDF profile", "email = "+  extras.getString("email"));
//                //Log.d("ASDF profile", "password = "+  extras.getString("password"));
//
//                //Log.d("ASDF profile", "gender = " + extras.getString("gender"));
//
//                //Log.d("ASDF profile", "genres = " + extras.getString("genres"));

                UserDatabase userDatabase = new UserDatabase(this);
                try {
                    JSONArray genresJSON = new JSONArray(extras.getString("genres"));
                    ArrayList<String> genres = new ArrayList<>();

                    for(int i = 0 ;  i < genresJSON.length() ; i++){
                        genres.add(genresJSON.getString(i));
                    }

                    userDatabase.addNewUser(new CustomUser(extras.getString("username"), extras.getString("password"), extras.getString("email"), extras.getString("gender")));
                    userDatabase.initializeGenreToUser(extras.getString("username"), genres);

                    userDatabase.setActiveUser(extras.getString("username"), false, false, false);

                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);

//                Toast.makeText(this, "Welcome new user", Toast.LENGTH_SHORT).show();
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }

    }
}
