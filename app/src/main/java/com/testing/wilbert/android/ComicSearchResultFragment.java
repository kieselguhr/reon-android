package com.testing.wilbert.android;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ComicSearchResultFragment extends Fragment {


    public ComicSearchResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comic_search_result, container, false);
    }

    public static ComicSearchResultFragment newInstance(Context context, String query) {
        ComicSearchResultFragment comicSearchResultFragment = new ComicSearchResultFragment();
        comicSearchResultFragment.setContext(context);
        comicSearchResultFragment.setComicModel(new ComicModel(context));

        return comicSearchResultFragment;
    }

    Context context;
    ComicModel comicModel;
    String query;

    ArrayList<Comic> queryResult = new ArrayList<>();

    @Nullable
    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ComicModel getComicModel() {
        return comicModel;
    }

    public void setComicModel(ComicModel comicModel) {
        this.comicModel = comicModel;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
        if (query.equals("")) {
            queryResult = new ArrayList<>();
        } else {
            queryResult = comicModel.searchComicBy(query, null, null);
        }

        if (comicResultAdapter != null) {
            comicResultAdapter.setComicNodes(queryResult);
            comicResultAdapter.notifyDataSetChanged();
        }

    }

    @BindView(R.id.comic_search_result_frag__recycler_view)
    RecyclerView recyclerView;
    ComicResultAdapter comicResultAdapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        comicResultAdapter = new ComicResultAdapter(queryResult);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(comicResultAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

    }
}


class ComicResultAdapter extends RecyclerView.Adapter<SearchComicNodeViewHolder> {

    ArrayList<Comic> comicNodes;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public ComicResultAdapter(ArrayList<Comic> comicNodes) {
        this.comicNodes = comicNodes;
    }

    public ArrayList<Comic> getComicNodes() {
        return comicNodes;
    }

    public void setComicNodes(ArrayList<Comic> comicNodes) {
        this.comicNodes = comicNodes;
    }

    @NonNull
    @Override
    public SearchComicNodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.row_comic_search_result, parent, false);
        return new SearchComicNodeViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchComicNodeViewHolder holder, final int position) {
        holder.coverImage.setImageDrawable(mContext.getDrawable(comicNodes.get(position).getCoverID()));
        holder.comicTitleTextView.setText(comicNodes.get(position).getTitle());

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, ComicDetailActivity.class);

                Bundle extras = new Bundle();
                extras.putString("comic", ComicModel.convertComicToJSONObject(comicNodes.get(position)).toString());
                intent.putExtras(extras);

                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {

        return comicNodes.size();
    }
}

class SearchComicNodeViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.image_strip_col__text)
    TextView comicTitleTextView;

    @BindView(R.id.image_strip_col__cover_image)
    ImageView coverImage;

    @BindView(R.id.image_strip_col__view)
    View rootView;

    Context mContext;

    public SearchComicNodeViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}
