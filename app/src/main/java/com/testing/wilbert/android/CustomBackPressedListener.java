package com.testing.wilbert.android;

public interface CustomBackPressedListener {
    void onBackButtonPressed();
}
