package com.testing.wilbert.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyAccountActivity extends AppCompatActivity {

    private static final int EDIT_PROFILE_ACTIVITY = 122;
    @BindView(R.id.my_account_act__email_text)
    TextView emailText;

    @BindView(R.id.my_account_act__favourite_genre_text)
    TextView genreText;

    @BindView(R.id.my_account_act__username_text)
    TextView usernameText;

    @OnClick(R.id.my_account_act__edit_password_btn)
    void callChangePassword(){
        PasswordEditPopUpFragment passwordEditPopUpFragment = PasswordEditPopUpFragment.newInstance(new PasswordEditPopUpFragment.PasswordChangedListener() {
            @Override
            public void onPasswordChanged(String password) {
                Toast.makeText(MyAccountActivity.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();

            }
        });
        passwordEditPopUpFragment.show(getSupportFragmentManager(), "password_change");

    }

    @BindView(R.id.my_account_act__profile_image)
    CircleImageView profileImage;

    @OnClick(R.id.my_account_act__sign_out_btn)
    void signOut() {

        setResult(ACTIVITY_RESULT_LOGOUT);
        finish();

    }

    @OnClick(R.id.my_account_act__edit_profile_btn)
    void editProfile() {
        Intent intent = new Intent(this, EditProfileActivity.class);
        startActivityForResult(intent, EDIT_PROFILE_ACTIVITY);
    }

    public static final int ACTIVITY_RESULT_LOGOUT = 6;
    public static final int ACTIVITY_RESULT_NONE = 1;

    final int defaultProfileImage[] = {
            R.id.register_gender_act__girl_icon,
            R.id.register_gender_act__boy_icon
    };

    UserDatabase userDatabase;

    public boolean onOptionsItemSelected(MenuItem item){
        this.finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
        userDatabase = new UserDatabase(this);
        getSupportActionBar().setTitle("My Account");

        CustomUser customUser = userDatabase.getActiveUser();

        if (customUser.getGender().equals("boy")) {
            profileImage.setImageResource(R.drawable.reon_boy_icon);
        } else {
            profileImage.setImageResource(R.drawable.reon_girl_icon);
        }

        usernameText.setText(customUser.getUsername());
        emailText.setText(customUser.getEmail());
        genreText.setText(userDatabase.getUserGenres(userDatabase.getActiveUsername()).toString());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == EDIT_PROFILE_ACTIVITY){
            recreate();
        }

    }
}
