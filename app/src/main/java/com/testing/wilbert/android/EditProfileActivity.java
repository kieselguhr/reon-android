package com.testing.wilbert.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {

    private static final int REGISTER_GENRE_ACTIVITY = 241;
    @BindView(R.id.edit_account_act__email_text)
    TextView emailText;

    @BindView(R.id.edit_account_act__favourite_genre_text)
    TextView genreText;

    @BindView(R.id.edit_account_act__username_text)
    TextView usernameText;

    @BindView(R.id.edit_account_act__profile_image)
    CircleImageView profileImage;

    UserDatabase userDatabase;
    JSONArray newFavouriteGenres;

    @OnClick(R.id.edit_account_act__change_genres_btn)
    void changeGenre(){
        Intent intent = new Intent(this, RegisterGenreActivity.class);

        Bundle extras = new Bundle();

        if(newFavouriteGenres != null){
            extras.putString("genres", newFavouriteGenres.toString());
        }else{
            extras.putString("genres", new JSONArray(customUser.getGenres()).toString());
        }

        intent.putExtras(extras);

        startActivityForResult(intent, REGISTER_GENRE_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REGISTER_GENRE_ACTIVITY){

            if(resultCode == RESULT_OK){
                try {
                    Bundle extras = data.getExtras();
                    newFavouriteGenres = new JSONArray(extras.getString("genres"));

                    ArrayList<String> arrayList = UserDatabase.covertJSONArraytoArrayList(newFavouriteGenres);
                    if(arrayList != null){
                        genreText.setText(arrayList.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    @OnClick(R.id.edit_account_act__save_changes_btn)
    void saveChanges(){
        userDatabase.editUser(userID, emailText.getText().toString(), usernameText.getText().toString(), null, newFavouriteGenres);
        userDatabase.setActiveUser(usernameText.getText().toString(), userDatabase.isPasswordRemembered(), userDatabase.isAutoLogin(), userDatabase.isGoogle());

        finish();
    }

    int userID;
    CustomUser customUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getSupportActionBar().setTitle("Edit Profile");

        userDatabase = new UserDatabase(this);

        ButterKnife.bind(this);

        customUser= userDatabase.getActiveUser();

        userID = userDatabase.findUserIDbyEmail(customUser.getEmail());

        if(customUser.getGender().equals("boy")){
            profileImage.setImageResource(R.drawable.reon_boy_icon);
        }else{
            profileImage.setImageResource(R.drawable.reon_girl_icon);
        }

        usernameText.setText(customUser.getUsername());
        emailText.setText(customUser.getEmail());
        genreText.setText(userDatabase.getUserGenres(userDatabase.getActiveUsername()).toString());

    }
}
