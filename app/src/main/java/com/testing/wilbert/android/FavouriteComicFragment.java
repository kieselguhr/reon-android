package com.testing.wilbert.android;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavouriteComicFragment extends Fragment {


    @BindView(R.id.favourite_comic_frag__title)
    TextView titleText;

    @BindView(R.id.favourite_comic_frag__comic_count)
    TextView comicCountText;

    @BindView(R.id.favourite_comic_frag__recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.favourite_comic_frag__empty_text)
    TextView emptyView;

    ArrayList<Comic> comics;
    View view;

    @BindView(R.id.favourite_comic_frag__change_order)
    TextView changeOrder;

    @OnClick(R.id.favourite_comic_frag__change_order)
    void changeOrder() {

        String status = changeOrder.getText().toString();

        if(status.contains("Updated")){

            changeOrder.setText("Sort By : Alphabetical");

            comics = ComicModel.sortComicAlhpabetically(comics);

            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();


        }else if(status.contains("Alphabetical")){

            changeOrder.setText("Sort By : Updated");
            comics = comicModel.getComicsByIds(userDatabase.getFavouriteComicsId(userDatabase.getActiveUsername()));
            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();

        }

    }


    public static FavouriteComicFragment newInstance(Context context){
        FavouriteComicFragment favouriteComicFragment = new FavouriteComicFragment();
        favouriteComicFragment.setComicModel(new ComicModel(context));
        favouriteComicFragment.setUserDatabase(new UserDatabase(context));
        return favouriteComicFragment;
    }

    public FavouriteComicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favourite_comic, container, false);
    }

    ComicModel comicModel;
    UserDatabase userDatabase;
    ImageStripDetailAdapter imageStripDetailAdapter;

    @Override
    public void onStart() {
        super.onStart();
        refresh();
        //Log.d("ASDF", "start is called");

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        this.view = view;
        titleText.setText("Favourites");

    }

    public void refresh(){
        ////Log.d("ASDF", "refresh is called");
        userDatabase.refreshUser();
        comics = comicModel.getComicsByIds(userDatabase.getFavouriteComicsId(userDatabase.getActiveUsername()));

        if (comics.size() > 0) {
            emptyView.setVisibility(View.GONE);
        }

        comicCountText.setText(comics.size() + " Comics");

        imageStripDetailAdapter = new ImageStripDetailAdapter(comics);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(imageStripDetailAdapter);

        recyclerView.setLayoutManager(gridLayoutManager);
        imageStripDetailAdapter.notifyDataSetChanged();


    }

    public void setComicModel(ComicModel comicModel) {
        this.comicModel = comicModel;
    }

    public void setUserDatabase(UserDatabase userDatabase) {
        this.userDatabase = userDatabase;
    }
}
