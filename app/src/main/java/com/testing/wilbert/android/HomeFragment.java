package com.testing.wilbert.android;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }

    private MainActivity mainActivity;
    private int positionToBeChanged;


    private CustomBackPressedListener backPressedListener = null;

    public void swapToAImageStripDetailFragment(String title, int mode){
//        Toast.makeText(getContext(), "SWAPPING", Toast.LENGTH_SHORT).show();

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        final ImageStripDetailFragment imageStripDetailFragment = ImageStripDetailFragment.newInstance(title, mode);
        ft.replace(R.id.home_frag__root, imageStripDetailFragment);
        ft.commit();

        backPressedListener = new CustomBackPressedListener() {
            @Override
            public void onBackButtonPressed() {
                imageStripDetailFragment.backListener();
//                backPressedListener = null;
            }
        };

        if(getActivity() instanceof MainActivity){
            ((MainActivity) getActivity()).setCustomBackPressedListener(backPressedListener);
        }

    }


    public static HomeFragment newInstance(MainActivity mainActivity, int positionToBeChanged){

        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setMainActivity(mainActivity);
        homeFragment.setPositionToBeChanged(positionToBeChanged);

        return homeFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    int[] sampleImages = {R.drawable.ad_banner, R.drawable.banner_youtube, R.drawable.banner_preview, R.drawable.banner_berlangganan};

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        CarouselView carouselView = (CarouselView) view.findViewById(R.id.home_frag__ad_carousel);

        carouselView.setImageListener(new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                imageView.setImageResource(sampleImages[position]);
//                imageView.setAdjustViewBounds(true);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            }
        });

        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {

                if(position == 0){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.agatestudio.reonmatchthree&hl=en_US"));
                    startActivity(browserIntent);
                }

                if(position == 1){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=0-2lbERuV8g"));
                    startActivity(browserIntent);
                }

                if(position == 2){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.reoncomics.com/previewkomik.pdf"));
                    startActivity(browserIntent);
                }

                if(position == 3){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.reoncomics.com/help/shopping/berlangganan"));
                    startActivity(browserIntent);
                }
            }
        });

        carouselView.setPageCount(sampleImages.length);

        ComicModel comicModel = new ComicModel(getContext());

        ArrayList<Comic> editorChociceArray = comicModel.getEditorChoiceComic();

        UserDatabase userDatabase = new UserDatabase(getContext());

        ArrayList<String> favouriteGenres = userDatabase.getUserGenres(userDatabase.getActiveUsername());

        ArrayList<Comic> reccomendedComicArray = comicModel.getReccomendedComic(favouriteGenres);
        Collections.shuffle(reccomendedComicArray);

        ArrayList<Comic> newestComicArray = comicModel.getNewestUpdateComic();

        ImageStripFragment editorChoiceFrag = ImageStripFragment.newInstance("Editor's Choice", ImageStripFragment.IMAGE_STRIP_FRAGMENT_MODE_EDITOR_CHOICE, this);
        ImageStripFragment reccomendedFrag = ImageStripFragment.newInstance("Reccomended For You", ImageStripFragment.IMAGE_STRIP_FRAGMENT_MODE_RECCOMENDED, this);
        ImageStripFragment newestUpdateFrag = ImageStripFragment.newInstance("Newest Update", ImageStripFragment.IMAGE_STRIP_FRAGMENT_MODE_NEWEST_UPDATE, this);



        FragmentTransaction ft = getFragmentManager().beginTransaction();



        ft.replace(R.id.home_frag__editors_choice_strip, editorChoiceFrag);
        ft.replace(R.id.home_frag__recommended_strip, reccomendedFrag);
        ft.replace(R.id.home_frag__newest_update, newestUpdateFrag);
        ft.commit();


//        editorChoiceFrag.setComics(editorChociceArray);


    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public void setPositionToBeChanged(int positionToBeChanged) {
        this.positionToBeChanged = positionToBeChanged;
    }

    public CustomBackPressedListener getBackPressedListener() {
        return backPressedListener;
    }
}


