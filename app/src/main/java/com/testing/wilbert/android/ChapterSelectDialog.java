package com.testing.wilbert.android;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChapterSelectDialog extends DialogFragment {

    int chapters[];

    int chapterCount;
    int currentPosition;
    OnChapterSelectedListener onChapterSelectedListener;

    public interface OnChapterSelectedListener {
        void onChapterSelected(int selectedChapter);
    }

    public static ChapterSelectDialog newInstance(int chapterCount, int currentPosition, OnChapterSelectedListener onChapterSelectedListener) {
        ChapterSelectDialog chapterSelectDialog = new ChapterSelectDialog();
        chapterSelectDialog.setChapterCount(chapterCount);
        chapterSelectDialog.setOnChapterSelectedListener(onChapterSelectedListener);
        chapterSelectDialog.setCurrentPosition(currentPosition);
        return chapterSelectDialog;
    }

    public static ChapterSelectDialog newInstance(int chapters[], int currentPosition, OnChapterSelectedListener onChapterSelectedListener) {
        ChapterSelectDialog chapterSelectDialog = new ChapterSelectDialog();
        chapterSelectDialog.setChapters(chapters);
        chapterSelectDialog.setOnChapterSelectedListener(onChapterSelectedListener);
        chapterSelectDialog.setCurrentPosition(currentPosition);
        return chapterSelectDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_select_chapter, container, false);
    }

    @BindView(R.id.chapter_select_dialog__recycler_view)
    RecyclerView recyclerView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        ReaderChapterSelectAdapter readerChapterSelectAdapter;
        if(chapters != null){
            readerChapterSelectAdapter = new ReaderChapterSelectAdapter(chapters, this, currentPosition);
        }else{
            readerChapterSelectAdapter = new ReaderChapterSelectAdapter(chapterCount, this, currentPosition);
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);


        recyclerView.setAdapter(readerChapterSelectAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);
    }

    public void setChapterCount(int chapterCount) {
        this.chapterCount = chapterCount;
    }


    public void setOnChapterSelectedListener(OnChapterSelectedListener onChapterSelectedListener) {
        this.onChapterSelectedListener = onChapterSelectedListener;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public OnChapterSelectedListener getOnChapterSelectedListener() {
        return onChapterSelectedListener;
    }

    public void setChapters(int[] chapters) {
        this.chapters = chapters;
    }
}

class ReaderChapterSelectViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.chapter_row_chapter__title)
    TextView chapterName;

    @BindView(R.id.chapter_row_chapter__radio)
    RadioButton radioButton;

    @BindView(R.id.chapter_row_chapter__view)
    View view;


    public ReaderChapterSelectViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

class ReaderChapterSelectAdapter extends RecyclerView.Adapter<ReaderChapterSelectViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ChapterSelectDialog chapterSelectDialog;

    private int chapterCount;
    private int currentPosition;

    private int chapters[];

    public ReaderChapterSelectAdapter(int chapterCount, ChapterSelectDialog chapterSelectDialog, int currentPosition) {
        this.chapterCount = chapterCount;
        this.chapterSelectDialog = chapterSelectDialog;
        this.currentPosition = currentPosition;
    }

    public ReaderChapterSelectAdapter(int chapters[], ChapterSelectDialog chapterSelectDialog, int currentPosition) {
        this.chapters = chapters;
        this.chapterSelectDialog = chapterSelectDialog;
        this.currentPosition = currentPosition;
    }

    @NonNull
    @Override
    public ReaderChapterSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.row_chapter_read_select, parent, false);
        return new ReaderChapterSelectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReaderChapterSelectViewHolder holder, final int position) {

        if (chapters == null) {
            holder.chapterName.setText("Chapter " + (position + 1));

            if (position == currentPosition) {
                holder.radioButton.setChecked(true);
                holder.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chapterSelectDialog.dismiss();
                    }
                });
            } else {
                holder.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chapterSelectDialog.getOnChapterSelectedListener().onChapterSelected(position);
                        chapterSelectDialog.dismiss();
                    }
                });
            }

        } else {
            holder.chapterName.setText("Chapter " + chapters[position]);

            if (chapters[position] == currentPosition) {
                holder.radioButton.setChecked(true);
                holder.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chapterSelectDialog.dismiss();
                    }
                });
            } else {
                holder.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chapterSelectDialog.getOnChapterSelectedListener().onChapterSelected(position);
                        chapterSelectDialog.dismiss();
                    }
                });
            }


        }


    }

    @Override
    public int getItemCount() {
        if (chapters != null) {
            return chapters.length;
        }
        else{
            return chapterCount;
        }
    }

    public void setChapterCount(int chapterCount) {
        this.chapterCount = chapterCount;
    }
}

