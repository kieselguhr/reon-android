package com.testing.wilbert.android;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class MyApplication extends Application {

    private CustomDownloadManager customDownloadManager;

    @Override
    public void onCreate() {
        super.onCreate();
        customDownloadManager = CustomDownloadManager.newInstance(this);
//        registerReceiver()
    }

    public CustomDownloadManager getCustomDownloadManager() {
        return customDownloadManager;
    }

    public void setCustomDownloadManager(CustomDownloadManager customDownloadManager) {
        this.customDownloadManager = customDownloadManager;
    }
}
