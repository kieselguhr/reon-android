package com.testing.wilbert.android;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageStripDetailFragment extends Fragment {

    @BindView(R.id.image_strip_detail_act__title)
    TextView titleText;

    @BindView(R.id.image_strip_detail_act__comic_count)
    TextView comicCountText;

    @BindView(R.id.image_strip_detail_act__recycler_view)
    RecyclerView recyclerView;


    @BindView(R.id.image_strip_detail_act__change_order)
    TextView changeOrder;

    ArrayList<Comic> comics;
    ArrayList<Comic> originalOrder;

    private String title;
    private int mode;

    @OnClick(R.id.image_strip_detail__back_btn)
    void back(){
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.image_strip_detail_frag__root_view, new HomeFragment());
        ft.commit();
    }

    public void backListener(){
//        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        ft.replace(R.id.image_strip_detail_frag__root_view, new HomeFragment());
//        ft.commit();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.image_strip_detail_frag__root_view, new HomeFragment());
        ft.commit();
    }

    public static ImageStripDetailFragment newInstance(String title, int mode){
        ImageStripDetailFragment imageStripDetailFragment = new ImageStripDetailFragment();
        imageStripDetailFragment.setTitle(title);
        imageStripDetailFragment.setMode(mode);

        return imageStripDetailFragment;
    }

    @OnClick(R.id.image_strip_detail_act__change_order)
    void changeOrder() {

        String status = changeOrder.getText().toString();

        if(status.contains("Updated")){

            if(originalOrder==null){
                originalOrder = (ArrayList<Comic>) comics.clone();
            }

            changeOrder.setText("Sort By : Alphabetical");

            comics = ComicModel.sortComicAlhpabetically(comics);

            imageStripDetailAdapter.setComicNodes(comics);
            imageStripDetailAdapter.notifyDataSetChanged();


        }else if(status.contains("Alphabetical")){

            changeOrder.setText("Sort By : Updated");
            imageStripDetailAdapter.setComicNodes(originalOrder);
            imageStripDetailAdapter.notifyDataSetChanged();

        }

    }

    UserDatabase userDatabase;
    ImageStripDetailAdapter imageStripDetailAdapter;

    public ImageStripDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_strip_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

//        //Log.d("ASDF","Title is " + title);


    }

    @Override
    public void onStart() {
        super.onStart();


        userDatabase = new UserDatabase(getContext());

        ComicModel comicModel = new ComicModel(getContext());
        UserDatabase userDatabase = new UserDatabase(getContext());

        switch (mode){
            case ImageStripFragment.IMAGE_STRIP_FRAGMENT_MODE_EDITOR_CHOICE:
                comics = comicModel.getEditorChoiceComic();
                title = "Editor's Choice";
                break;
            case ImageStripFragment.IMAGE_STRIP_FRAGMENT_MODE_RECCOMENDED:
                comics = comicModel.getReccomendedComic(userDatabase.getUserGenres(userDatabase.getActiveUsername()));
                title = "Recommended";
                break;
            case ImageStripFragment.IMAGE_STRIP_FRAGMENT_MODE_NEWEST_UPDATE:
                comics = comicModel.getNewestUpdateComic();
                title = "Newest Update";
                break;
            default:
                comics = new ArrayList<>();
        }
        comicCountText.setText(comics.size() + " Comics");
        titleText.setText(title);

        imageStripDetailAdapter = new ImageStripDetailAdapter(comics);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(imageStripDetailAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setComics(ArrayList<Comic> comics) {
        this.comics = comics;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
}
