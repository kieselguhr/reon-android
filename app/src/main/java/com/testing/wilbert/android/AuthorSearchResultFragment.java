package com.testing.wilbert.android;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class AuthorSearchResultFragment extends Fragment {


    public AuthorSearchResultFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.author_search_result_frag__recycler_view)
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_author_search_result, container, false);
    }


    public static AuthorSearchResultFragment newInstance(Context context, String query){
        AuthorSearchResultFragment authorSearchResultFragment = new AuthorSearchResultFragment();
        authorSearchResultFragment.setContext(context);
        authorSearchResultFragment.setComicModel(new ComicModel(context));

        return authorSearchResultFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        authorResultAdapter = new AuthorResultAdapter(queryResult);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false);

        recyclerView.setAdapter(authorResultAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);


    }

    String query;
    ArrayList<String> queryResult = new ArrayList<>();
    ComicModel comicModel;
    Context context;

    AuthorResultAdapter authorResultAdapter;

    @Nullable
    @Override
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ComicModel getComicModel() {
        return comicModel;
    }

    public void setComicModel(ComicModel comicModel) {
        this.comicModel = comicModel;
    }

    public AuthorResultAdapter getAuthorResultAdapter() {
        return authorResultAdapter;
    }

    public void setAuthorResultAdapter(AuthorResultAdapter authorResultAdapter) {
        this.authorResultAdapter = authorResultAdapter;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
        if(query.equals("")){
            queryResult = new ArrayList<>();
        }else{
            queryResult = comicModel.searchAuthorBy(query);
        }

        if (authorResultAdapter != null) {
            authorResultAdapter.setAuthors(queryResult);
            authorResultAdapter.notifyDataSetChanged();
        }

    }
}

class SearchAuthorNodeViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.author_result__user_name)
    TextView authorTitleTextView;

    @BindView(R.id.author_result__user_image)
    ImageView profileImage;

    @BindView(R.id.author_result__view)
    View rootView;

    Context mContext;

    public SearchAuthorNodeViewHolder(View itemView, Context mContext) {
        super(itemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }
}

class AuthorResultAdapter extends RecyclerView.Adapter<SearchAuthorNodeViewHolder> {

    ArrayList<String> authors;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public AuthorResultAdapter(ArrayList<String> authors){
        this.authors = authors;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    @NonNull
    @Override
    public SearchAuthorNodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;

        view = mLayoutInflater.inflate(R.layout.row_author_search_result, parent, false);
        return new SearchAuthorNodeViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAuthorNodeViewHolder holder, final int position) {
        holder.authorTitleTextView.setText(authors.get(position));

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ImageStripDetailActivity.class);
                Bundle extras = new Bundle();

                ComicModel comicModel = new ComicModel(mContext);

                extras.putString("title", authors.get(position));
                extras.putString("comics", ComicModel.convertComicArrayListToJSONArray(comicModel.searchComicBy(null, authors.get(position), null)).toString());
                intent.putExtras(extras);

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

        return authors.size();
    }
}
